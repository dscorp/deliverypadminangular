import {HttpClientModule} from '@angular/common/http';
import {AngularFirestore} from '@angular/fire/firestore';
import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {APP_ROUTES} from './app.routing';
import {ContenedorprincipalComponent} from './presentation/DashBoardAdminEstablecimientos/contenedorprincipal.component';
import {MatTabsModule} from '@angular/material/tabs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {ListComponent} from './components/paginas/administacion/listarPedidosAdministracion/list/list.component';
import {MatButtonModule} from '@angular/material/button';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';
import {MatListModule} from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';
import {MatMenuModule} from '@angular/material/menu';
import {ListarproductosdeestablecimientoComponent} from './components/paginas/administacion/listarproductosdeestablecimiento/listarproductosdeestablecimiento.component';
import {PedidosporconfirmarComponent} from './presentation/DashBoardControlPedidos/PedidosPorConfirmar/pedidosporconfirmar.component';
import {OrdenesTransportAatendidasComponent} from './presentation/DashBoardControlPedidos/OrdenesTransporteAtendidas/OrdenesTransportAatendidas.component';
import {PedidosEnEstablecimientoComponent} from './presentation/DashBoardControlPedidos/PedidosAceptados/PedidosEnEstablecimiento.component';
import {CatalogoComponent} from './components/catalogo/catalogo.component';
import {MatBadgeModule} from '@angular/material/badge';
import {NavbarComponent} from './components/catalogo/navbar/navbar.component';
import {FooterComponent} from './components/catalogo/footer/footer.component';
import {MatCardModule, MatDialogModule, MatRadioModule, MatSidenavModule} from '@angular/material';
import {ConcretarpedidoComponent} from './components/catalogo/concretarpedido/concretarpedido.component';
import {ResumenventasComponent} from './components/paginas/administacion/resumenventas/resumenventas.component';
import {LoginComponent} from './components/catalogo/login/login.component';
import {AngularFireAuth} from '@angular/fire/auth';
import {TlfComponent} from './components/catalogo/tlf/tlf.component';
import {TelefonodialogComponent} from './components/catalogo/login/telefonodialog/telefonodialog.component';
import {ListaapoyosComponent} from './presentation/DashBoardControlPedidos/DEPRECATEDLstApoyos/listaapoyos.component';
import {DialogregistroapoyoComponent} from './presentation/DashBoardControlPedidos/DEPRECATEDLstApoyos/dialogregistroapoyo/dialogregistroapoyo.component';
import {ListarPromocionesComponent} from './components/paginas/administacion/listarPedidosAdministracion/listar-promociones/listar-promociones.component';
import {DialogRegistrarPromocionComponent} from './components/paginas/administacion/listarPedidosAdministracion/listar-promociones/dialog-registrar-promocion/dialog-registrar-promocion.component';
import {DialogModificarProductoComponent} from './components/paginas/administacion/listarproductosdeestablecimiento/dialog-modificar-producto/dialog-modificar-producto.component';
import {DatePipe, DecimalPipe, registerLocaleData} from '@angular/common';
import localePe from '@angular/common/locales/es-PE';
import {MapcourierComponent} from './presentation/DashBoardControlPedidos/MapaRepartidores/mapcourier.component';
import {AgmCoreModule} from "@agm/core";
import {DialogInfroRepartidorComponent} from './presentation/DashBoardControlPedidos/MapaRepartidores/dialog-infro-repartidor/dialog-infro-repartidor.component';
import {DiaogAsignarordentransporteComponent} from './presentation/DashBoardControlPedidos/OrdenesTransporteAtendidas/diaog-asignarordentransporte/diaog-asignarordentransporte.component';
import {DialogOrdenTransporteApoyoComponent} from './presentation/DashBoardControlPedidos/OrdenesTransporteAtendidas/dialog-orden-transporte-apoyo/dialog-orden-transporte-apoyo.component';
import {OrdenTransporteRepository} from "./domain/repository/OrdenTransporteRepository";
import {OrdenTransporteRepositoryImpl} from "./data/repository/OrdenTransporteRepositoryImpl";
import {PedidoRepository} from "./domain/repository/PedidoRepository";
import {PedidoRepositoryImpl} from "./data/repository/PedidoRepositoryImpl";
import {EstablecimientoRepository} from "./domain/repository/EstablecimientoRepository";
import {RepartidorRepository} from "./domain/repository/RepartidorRepository";
import {RepartidorRepositoryImpl} from "./data/repository/RepartidorRepositoryImpl";
import {LugarEntregaRepository} from "./domain/repository/LugarEntregaRepository";
import {LugarEntregaRepositoryImpl} from "./data/repository/LugarEntregaRepositoryImpl";
import {EstablecimientoRepositoryImpl} from "./data/repository/EstablecimientoRepositoryImpl";
import {DashBoardControlPedidosComponent} from './presentation/DashBoardControlPedidos/dash-board-control-pedidos.component';
import {PedidosPorConfirmarModel} from "./data/Model/dashboardControlPedidos/PedidosPorConfirmarModel";
import {PedidosPorConfirmarPresenter} from "./presentation/DashBoardControlPedidos/PedidosPorConfirmar/PedidosPorConfirmarPresenter";
import {PedidosAceptadosMVP} from "./data/mvpDefinitions/dashboardControlPedidos/PedidosAceptadosMVP";
import {PedidosAceptadosPresenter} from "./presentation/DashBoardControlPedidos/PedidosAceptados/PedidosAceptadosPresenter";
import {PedidosAceptadosModel} from "./data/Model/dashboardControlPedidos/PedidosAceptadosModel";
import {OrdenesTransporteAtendidasMVP} from "./data/mvpDefinitions/dashboardControlPedidos/OrdenesTransporteAtendidasMVP";
import {OrdenTransporteAtendidaPresenter} from "./presentation/DashBoardControlPedidos/OrdenesTransporteAtendidas/OrdenTransporteAtendidaPresenter";
import {OrdenTranporteAtendidaModel} from "./data/Model/dashboardControlPedidos/OrdenTransporteAtendidaModel";
import {PedidosPorConfirmarMVP} from "./data/mvpDefinitions/dashboardControlPedidos/PedidosPorConfirmarMVP";
import {MatSortModule} from "@angular/material/sort";
import {DashBoardAdminEstablecimientosModel} from "./data/Model/dashboardAdminEstablecimientos/DashBoardAdminEstablecimientosModel";
import {PedidosRechazadosComponent} from "./presentation/DashBoardControlPedidos/PedidosRechazados/pedidos-rechazados.component";
import {PedidosRechazadosMVP} from "./data/mvpDefinitions/dashboardControlPedidos/PedidosRechazadosMVP";
import {PedidosRechazadosModel} from "./data/Model/dashboardControlPedidos/PedidosRechazadosModel";
import {PedidosRechazadosPresenter} from "./presentation/DashBoardControlPedidos/PedidosRechazados/PedidosRechazadosPresenter";

registerLocaleData(localePe, 'es-PE')

@NgModule({
  declarations: [
    AppComponent,
    ContenedorprincipalComponent,
    ListComponent,
    PedidosporconfirmarComponent,
    OrdenesTransportAatendidasComponent,
    PedidosEnEstablecimientoComponent,
    ListarproductosdeestablecimientoComponent,
    CatalogoComponent,
    NavbarComponent,
    FooterComponent,
    ConcretarpedidoComponent,
    ResumenventasComponent,
    LoginComponent,
    TlfComponent,
    TelefonodialogComponent,
    ListaapoyosComponent,
    DialogregistroapoyoComponent,
    ListarPromocionesComponent,
    DialogRegistrarPromocionComponent,
    DialogModificarProductoComponent,
    MapcourierComponent,
    DialogInfroRepartidorComponent,
    DiaogAsignarordentransporteComponent,
    DialogOrdenTransporteApoyoComponent,
    DashBoardControlPedidosComponent,
    PedidosRechazadosComponent,
  ],
  imports: [
    SweetAlert2Module.forRoot(),
    FormsModule,
    MatListModule,
    MatSelectModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatIconModule,
    MatInputModule,
    HttpClientModule,
    AngularFireDatabaseModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule,
    MatBadgeModule,
    MatSidenavModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatDialogModule,
    MatRadioModule,
    APP_ROUTES,
    AngularFireModule.initializeApp(environment.firebase),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDDBKY6nbP_py1llBqM0rjE32uN5gNPd-U'
    }),
    MatSortModule
  ],
  entryComponents: [
    TelefonodialogComponent,
    DialogregistroapoyoComponent,
    DialogRegistrarPromocionComponent,
    DialogModificarProductoComponent,
    DialogInfroRepartidorComponent,
    DiaogAsignarordentransporteComponent,
    DialogOrdenTransporteApoyoComponent,
  ],
  exports: [
    CatalogoComponent,
  ],

  bootstrap: [AppComponent],
  providers: [
    {provide: LOCALE_ID, useValue: 'es-PE'},
    AngularFireAuth,
    AngularFirestore,
    DatePipe,
    DecimalPipe,

    //DI LIKE HILT
    //ORDEN TRANSPORTE
    {provide: OrdenTransporteRepository, useClass: OrdenTransporteRepositoryImpl},

    //PEDIDO
    {provide: PedidoRepository, useClass: PedidoRepositoryImpl},

    //ESTABLECIMIENTO

    {provide: EstablecimientoRepository, useClass: EstablecimientoRepositoryImpl},

    //REPARTIDOR

    {provide: RepartidorRepository, useClass: RepartidorRepositoryImpl},

    //LUGAR ENTREGA

    {provide: LugarEntregaRepository, useClass: LugarEntregaRepositoryImpl},

    //LUGAR ENTREGA


    //PEDIDOS POR CONFIRMAR MVP

    {provide: PedidosPorConfirmarMVP.Model, useClass: PedidosPorConfirmarModel},

    {provide: PedidosPorConfirmarMVP.Presenter, useClass: PedidosPorConfirmarPresenter},

    //PEDIDOS ACEPTADOS MVP

    {provide: PedidosAceptadosMVP.Model, useClass: PedidosAceptadosModel},
    {provide: PedidosAceptadosMVP.Presenter, useClass: PedidosAceptadosPresenter},


    //ORDENES TRANSPORTE ATENDIAS

    {provide: OrdenesTransporteAtendidasMVP.Presenter, useClass: OrdenTransporteAtendidaPresenter},
    {provide: OrdenesTransporteAtendidasMVP.Model, useClass: OrdenTranporteAtendidaModel},
    {provide: PedidosRechazadosMVP.Model, useClass: PedidosRechazadosModel},
    {provide: PedidosRechazadosMVP.Presenter, useClass: PedidosRechazadosPresenter},
    {provide: DashBoardAdminEstablecimientosModel, useClass: DashBoardAdminEstablecimientosModel}

  ]

})
export class AppModule {
}
