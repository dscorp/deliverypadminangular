import {RouterModule, Routes} from '@angular/router';
import {ContenedorprincipalComponent} from './presentation/DashBoardAdminEstablecimientos/contenedorprincipal.component';
import {CatalogoComponent} from './components/catalogo/catalogo.component';
import {ConcretarpedidoComponent} from './components/catalogo/concretarpedido/concretarpedido.component';
import {LoginComponent} from './components/catalogo/login/login.component';
import {TlfComponent} from './components/catalogo/tlf/tlf.component';
import {EnviarpedidoguardGuard} from './presentation/guards/enviarpedidoguard.guard';
import {ListarPromocionesComponent} from './components/paginas/administacion/listarPedidosAdministracion/listar-promociones/listar-promociones.component';
import {ListraPromocionesGuard} from './components/paginas/administacion/listarPedidosAdministracion/listra-promociones.guard';
import {ListarproductosdeestablecimientoComponent} from './components/paginas/administacion/listarproductosdeestablecimiento/listarproductosdeestablecimiento.component';
import {DashBoardControlPedidosComponent} from "./presentation/DashBoardControlPedidos/dash-board-control-pedidos.component";

const routes: Routes = [

  {
    path: '', redirectTo: 'catalogo', pathMatch: 'full'


  },
  {path: 'verpedidos123', component: DashBoardControlPedidosComponent},
  {
    path: 'listProductsByEstab', component: ListarproductosdeestablecimientoComponent,
    canActivate: [ListraPromocionesGuard],
  },




  {
    path: 'listPromossByEstab', component: ListarPromocionesComponent,
    canActivate: [ListraPromocionesGuard],
  },

  {

    path: 'admin', component: ContenedorprincipalComponent,
  },
  {path: 'tlf', component: TlfComponent},
  {
    path: 'enviarpedido', component: ConcretarpedidoComponent,
    canActivate: [EnviarpedidoguardGuard],
  },
  {path: 'login', component: LoginComponent},
  {

    path: 'catalogo',
    component: CatalogoComponent,
    //  canActivate: [AuthGuard],
    loadChildren: './components/catalogo/catalogo.module#CatalogoModule'

  }
];

export const APP_ROUTES = RouterModule.forRoot(routes);


// {
//   path: 'exubicacion',
//   children:
//   [
//     { path: '', component: ListarExamenUbicacionComponent },
//     { path: 'add', component: CrearExamenUbicacionComponent },
//     { path: ':id/postulantes' , component: AlumnosExamenUbicacionComponent},
//     { path: ':id/puntaje' , component: PuntajeExamenUbicacionComponent},
//     { path: ':id/asistencia' , component: AsistenciaExamenUbicacionComponent}
//   ]
// },
//   {

//     path: 'alumno',
//     children:
//     [
//       { path: '', component: ListarAlumnosComponent },
//       { path: 'colegio', component: AlumnoColegioComponent },
//       { path: 'colegio/:id', component: AlumnoColegioComponent },
//       { path: 'universidad', component: AlumnoUnivComponent },
//       { path: 'universidad/:id', component: AlumnoUnivComponent },
//       { path: 'test', component: RegistroUniversitarioOColegialComponent },
//     ]
//   },

//   {

//     path: 'docente',
//     children:
//     [
//       { path: '', component: ListarDocenteComponent },
//       { path: 'add', component: RegistrarDocenteComponent },
//       { path: ':id', component: RegistrarDocenteComponent }
//     ]
//   },

//   {
//     path: 'administrativo',
//     children:
//     [
//       { path: '', component: ListarAdministrativoComponent },
//       { path: 'add', component: RegistrarAdministrativoComponent },
//       { path: 'update/:id', component: RegistrarAdministrativoComponent }
//     ]
//   },

//   {
//     path: 'usuario',
//     children:
//     [
//       { path: 'alumno', component: ListarUsuarioComponent },
//       { path: 'docente', component: ListarUsuarioDocenteComponent },
//       { path: '', component: UserComponent },
//       { path: 'configuracion' , component: ConfiguracionUsuarioComponent }
//     ]
//   },
