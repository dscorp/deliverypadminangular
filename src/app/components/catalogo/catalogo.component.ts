import { MediaMatcher } from '@angular/cdk/layout';
import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild} from '@angular/core';
import { MatSidenav } from '@angular/material';
import { CategoriasPropiasestabService } from '../services/categorias-propiasestab.service';
import { PedidosService } from '../services/pedidos.service';
import { NavbarComponent } from './navbar/navbar.component';
import { PedidoDetalle } from 'src/app/domain/entity/pedidodetalle';
import { ProductoService } from '../services/producto.service';
import { map } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css']
})
export class CatalogoComponent implements OnInit,AfterViewInit {

  // establecimiento: establecimiento[];

  @ViewChild(NavbarComponent,null) navbar:NavbarComponent;

  @ViewChild(MatSidenav,null) matsidenav:MatSidenav;

  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;


  fillerNav = Array.from({length: 50}, (_, i) => `Nav Item ${i + 1}`);

  fillerContent = Array.from({length: 50}, () =>
      `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
       labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
       laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
       voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
       cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`);


  constructor(
                private categoriaEstablecimeinto: CategoriasPropiasestabService ,
                public pedidoService:PedidosService
                ,changeDetectorRef: ChangeDetectorRef,
                media: MediaMatcher,
                private productoService:ProductoService,
                private authService:AuthService) {
    this.mobileQuery = media.matchMedia('(max-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.InitValuesifSessionExist()
   }
  ngAfterViewInit(): void {
   this.navbar.sidenav=this.matsidenav;
  }

  ngOnInit() {

  }


  InitValuesifSessionExist()
  {
    if(this.authService.estaAutenticado())
    {
      console.log('nombre usuario logeado',this.authService.singedUser.nombreCompleto);

    }
  }

 getImageByIdProducto(idEstablecimiento:string,idProducto:string) {
  return this.productoService.getById(idEstablecimiento,idProducto).valueChanges().pipe(map(prod => prod.imagen));
 }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  incrementarCantidad = (pd:PedidoDetalle) => pd.cantidad<100?pd.cantidad+=1:pd.cantidad;

  decrementarCantidad = (pd:PedidoDetalle) => pd.cantidad>1?pd.cantidad-=1:1;

  eliminardp = (pd:PedidoDetalle) => this.pedidoService.eliminarPedidoDetalleEr(pd);
}
