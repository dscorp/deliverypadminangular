import {PanelestablecimientoreportComponent} from '../paginas/administacion/reports/panelestablecimientoreport/panelestablecimientoreport.component';
import {InformemensualComponent} from '../paginas/administacion/reports/informemensual/informemensual.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CATALOGO_ROUTES} from './catalogo.routing';
import {ListarEstablecimientosCatalaogoComponent} from './content-home/ListarEstablecimientosCatalaogo.component';
import {
  MatBadgeModule,
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatToolbarModule
} from '@angular/material';
import {ModalproductoComponent} from './lista-productos/modalproducto/modalproducto.component';
import {ListaProductosComponent} from './lista-productos/lista-productos.component';
import {FormsModule} from '@angular/forms';


@NgModule({


  declarations: [
    ListarEstablecimientosCatalaogoComponent,

    // ListaProductosComponent,
    // ListarproductosdeestablecimientoComponent
    ListaProductosComponent,
    ModalproductoComponent,
      InformemensualComponent,
      PanelestablecimientoreportComponent,

  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatToolbarModule,
    MatDialogModule,
    MatDividerModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MatFormFieldModule,
    MatIconModule,
    MatBadgeModule,
    MatListModule,
    FormsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatButtonModule,
    CATALOGO_ROUTES
  ],
  entryComponents: [
    ModalproductoComponent
  ],
})
export class CatalogoModule { }
