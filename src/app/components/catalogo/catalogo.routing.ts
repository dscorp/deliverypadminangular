import {InformemensualComponent} from '../paginas/administacion/reports/informemensual/informemensual.component';
import {RouterModule, Routes} from '@angular/router';
import {ListarEstablecimientosCatalaogoComponent} from './content-home/ListarEstablecimientosCatalaogo.component';
import {ListaProductosComponent} from './lista-productos/lista-productos.component';
import {ListarPromocionesComponent} from '../paginas/administacion/listarPedidosAdministracion/listar-promociones/listar-promociones.component';


const catalogoRoutes: Routes = [

  {path: '', component: ListarEstablecimientosCatalaogoComponent},
  {path: 'listarproductos/:idestablecimiento', component: ListaProductosComponent},
  {path: 'reportes', component: InformemensualComponent},


];


export const CATALOGO_ROUTES = RouterModule.forChild(catalogoRoutes);
