
import { Location } from '@angular/common';
import { Component, HostListener, Input, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { exception } from 'console';
import firebase, { firestore } from 'firebase';
import { LugarEntrega } from 'src/app/domain/entity/lugarEnttrega';
import { Pedido } from 'src/app/domain/entity/pedido';
import { PedidoDetalle } from 'src/app/domain/entity/pedidodetalle';
import { PedidoMultiple } from 'src/app/domain/entity/pedidomultimple';
import Swal from 'sweetalert2';
import { AuthService } from '../../services/auth.service';
import { LugarentregaserviceService } from '../../services/lugarentregaservice.service';
import { PedidosService } from '../../services/pedidos.service';


@Component({
  selector: 'app-concretarpedido',
  templateUrl: './concretarpedido.component.html',
  styleUrls: ['./concretarpedido.component.css']
})
export class ConcretarpedidoComponent implements OnInit {


  @Input() pedidoList: Array<PedidoMultiple>;

  lugaresEntrega = Array<LugarEntrega>();
  detalles: Array<PedidoDetalle> = new Array();
  costodelivery: number = 0;
  lugarEntregaSeleccionado;
  formEnvioPedido: FormGroup;
  subtotal: number;
  batch;

  @ViewChild('direccion',null) direccionInput: ElementRef;
  //FLAGS
  loadingFlag = false;

  constructor(private lugarEntregaService: LugarentregaserviceService,
    public pedidoService: PedidosService,
    private fbuilder: FormBuilder,
    private authService: AuthService,
    private location: Location,
    private firestore: AngularFirestore,
    private router: Router,


  ) {
    this.get();
    this.cargarDetallesPedidofromService();
    this.batch = this.firestore.firestore.batch();




    this.formEnvioPedido = fbuilder.group({
      "lugarEntrega": ['', Validators.required],
      "direccionEntrega": ['', Validators.required],
      "celular": ['', [Validators.pattern("^\\d{9}$"), Validators.required]],
    })

  }





  ngOnInit() {
    this.subtotal = this.pedidoService.calcularMontoTotal();

  }

  ngAfterViewInit(): void {


  //  console.log(this.direccionInput);
    this.direccionInput.nativeElement.addEventListener("click", ()=>{
    //  this.direccionInput.nativeElement.scrollIntoView();

     var y =   this.direccionInput.nativeElement.scrollHeight;
     var x =   this.direccionInput.nativeElement.scrollWidth;
     console.log(y);

     window.scrollTo(x, y+40)
      // y-=10

    }, false);



  }

  showLoadingDialog() {
    Swal.fire({
      title: 'Procesando pedido',
      allowOutsideClick: false,
      allowEscapeKey: false,
    })
    Swal.showLoading()
  }

  hideLoadingDialog() {
    Swal.close();
  }


  enviarPedido(form) {

    this.showLoadingDialog();


    if (this.pedidoService.pedidosList.length > 0) {

      this.pedidoService.pedidosList.forEach((pdmultiple: PedidoMultiple) => {

        var subtotalped = 0;
        pdmultiple.pedioDetalles.forEach((pedidoDetalle: PedidoDetalle) => {
          subtotalped += pedidoDetalle.cantidad * pedidoDetalle.precioVenta;
        });


        const pedido = new Pedido();
        //DATOS GENERALES
        var TheUniqueKeyOfPush = firebase.database().ref().push().key;
        pedido.id = TheUniqueKeyOfPush;
        // console.log('este es el id de firebase', pedido.id);
        pedido.estado = "porConfirmar";
        pedido.entregaInmediata = true;
        pedido.horaEntrega = '';
        pedido.pagoSinVuelto = true;
        pedido.pagaCon = '';
        pedido.motivoDeRechazo = '';
        pedido.created_at = new Date().toString();

        //SI SE VA A CREAR MAS DE 1 PEDIDO
        this.pedidoService.pedidosList.length > 1 ? pedido.isMultiple = true : pedido.isMultiple = false;

        pedido.lugarEntrega = ' ';

        //DATOS DINERO
        pedido.costoDelivery = this.costodelivery;
        pedido.subtotal = subtotalped;
        pedido.costoTotal = this.costodelivery + subtotalped;

        // //DATOS DE USUARIO
        // pedido.idUsers = this.authService.singedUser.id;
        // pedido.nombreCliente = this.authService.singedUser.nombreCompleto;
        // pedido.telefonoCliente = this.authService.singedUser.telefono;

        //DATOS DE ESTABLECIMIENTO
        pedido.idEstablecimiento = pdmultiple.idestablecimiento;
        pedido.nombreEstablecimiento = pdmultiple.nombreEstablecimiento;
        pedido.idEst_Estado = `${pdmultiple.idestablecimiento}_'porConfirmar'`


        var detallesString: string = "";
        pdmultiple.pedioDetalles.forEach((pd: PedidoDetalle) => {


          const precioventa = Math.round(pd.precioVenta);
          const cantidad= Math.round(pd.cantidad);



          const stringdata =(`${pd.cantidad}X ${pd.nombreProducto} = S/. ${precioventa * cantidad} \n`)
          detallesString = detallesString.concat(stringdata);
          var pdconDatosPedido = {
            ...pd,
            "fecha": pedido.created_at,
            "idEstablecimiento": pedido.idEstablecimiento,
            "nombreEstablecimiento": pedido.nombreEstablecimiento,
          }

          const id = this.firestore.createId();
          let pedidosDetallesRef = this.firestore.collection('PedidosDetalles').doc(id).ref;

          this.batch.set(pedidosDetallesRef, { ...pdconDatosPedido });


        })

        const id = this.firestore.createId();
        let pedidosRef: any = this.firestore.collection('Pedidos').doc(id).ref;
        this.batch.set(pedidosRef, {
          ...pedido,
          ...this.formEnvioPedido.value,
          "contenido": detallesString,
        });



        // console.table(this.formEnvioPedido.value);


        //IMPLEMENTACION PARA GUARDAR EN REALTIMEDATABASE Y MANDAR LOS PEDIDOS DIRECTAMENTE A LOS ESTABLECIMIENTOS
        // var atomic = {};


        // atomic[`Pedido/${pedido.id}`] = pedido;


        // pdmultiple.pedioDetalles.forEach((pd: PedidoDetalle) => {
        //   var idped = firebase.database().ref().push().key;
        //   atomic[`PedidoDetalle/${idped}`] = pd;
        // })


        // this.pedidoService.atomicSave(atomic).then(data => {
        //   console.log('pedidos registrados exitosamente');

        // }).catch(e => {
        //   console.log('error');
        //   console.log(e);
        // });



      });

      this.pedidoService.atomicSave(this.batch).then(data => {
        console.log('registro exitoso');

        this.hideLoadingDialog()

        Swal.fire({
          title: "Su pedido fué enviado correctamente",
          icon: 'success',
          text: ' En breve nos comunicaremos con usted al numero ' + this.formEnvioPedido.get('celular').value + ' para informarle el estado de su pedido',


        });
        this.pedidoService.pedidosList = new Array<PedidoMultiple>();
        this.router.navigate(['/catalogo'])

      }).catch(e => {
        Swal.fire({
          title: 'Ocurrion un error, vuelva a intentar mas tarde',
          text: `Error : ${e.message}`
        })
        // this.hideLoadingDialog();
      });

      this.batch = null;


    }



  }

  setCostoDelivery(index) {
    this.costodelivery = this.lugaresEntrega[index - 1].precio

    // var a=$event.target.value;

    // console.log(a);
  }

  cargarDetallesPedidofromService() {
    this.pedidoService.pedidosList.forEach(element => {
      element.pedioDetalles.forEach(element => {
        this.detalles.push(element);
      });
    });
  }

  get() {
    this.lugarEntregaService.getlugaresEntrega().subscribe(data => {
      this.lugaresEntrega = data;
    })
  }





  regresar() {
    this.location.back();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

}
