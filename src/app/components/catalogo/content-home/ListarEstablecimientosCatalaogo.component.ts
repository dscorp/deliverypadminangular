import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Establecimiento } from 'src/app/domain/entity/establecimiento';
import { EstablecimientoService } from '../../paginas/administacion/establecimiento.service';
import { CategoriasPropiasestabService } from '../../services/categorias-propiasestab.service';
import { pipe } from 'rxjs';
@Component({
  selector: 'app-content-home',
  templateUrl: './ListarEstablecimientosCatalaogo.component.html',
  styleUrls: ['./ListarEstablecimientosCatalaogo.css']
})
export class ListarEstablecimientosCatalaogoComponent implements OnInit {

  establecimiento: Establecimiento[];

  abierto: boolean = true;



  //FLAGS
  loadingFlag: boolean = false;



  constructor(private establecimientoService: EstablecimientoService,
    private categoriaEstablecimeinto: CategoriasPropiasestabService,
    private router: Router,) {



  }

  ngOnInit() {
    // this.getEstablecimientos()
    this.getEstablecimientosAPI();
  }





  getEstablecimientosAPI() {
    this.loadingFlag = true;

    this.establecimientoService.getestablecimientosApi().subscribe((data:[])=>{
      this.establecimiento = data
      this.loadingFlag = false;
    })


  }

  getEstablecimientos() {
    this.loadingFlag = true;
    this.establecimientoService.getEstablecimientos().valueChanges().subscribe(data => {
      this.establecimiento = data
      this.loadingFlag = false;
    })

  }

  onProductoClick(establecimiento: Establecimiento) {

    this.router.navigate(['catalogo/listarproductos', establecimiento.id]);


  }

}
