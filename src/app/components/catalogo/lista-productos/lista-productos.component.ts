import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Establecimiento } from 'src/app/domain/entity/establecimiento';
import { PedidoDetalle } from 'src/app/domain/entity/pedidodetalle';
import { PedidoDetalleEr } from 'src/app/domain/entity/pedidodetalleer';
import { Producto } from 'src/app/domain/entity/producto';
import { EstablecimientoService } from '../../paginas/administacion/establecimiento.service';
import { PedidosService } from '../../services/pedidos.service';
import { ProductoService } from '../../services/producto.service';
import { ModalproductoComponent } from './modalproducto/modalproducto.component';

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css']
})
export class ListaProductosComponent implements OnInit {

  productos: Producto[]
  establecimientos: Establecimiento[];

  cantidad: number = 0;
  pedidoDetalleSeleccionado: PedidoDetalleEr;
  productsToPedidoDetalle: Array<PedidoDetalleEr> = new Array();
  dialogRef: MatDialogRef<ModalproductoComponent, any>;

  idestablecimientoseleccionado: string;


  //FLAGS
  loadingFlag = false;

  constructor(private activateRoute: ActivatedRoute,
    private productosService: ProductoService,
    private establecimientoService: EstablecimientoService,
    private pedidoService: PedidosService,
    public dialog: MatDialog) { }

  ngOnInit() {

    this.idestablecimientoseleccionado = this.activateRoute.snapshot.params.idestablecimiento
    this.getproductos(this.idestablecimientoseleccionado);

  }

  openDialog(pd: PedidoDetalleEr): void {
    //asigarel pedidodetalle antes de abrir el dialogo para enviarlo al dialogo
    this.pedidoDetalleSeleccionado = pd;
    //abre el dialogo
    this.dialogRef = this.dialog.open(ModalproductoComponent, {
      panelClass: 'myapp-no-padding-dialog',
      width: '90%',
      height: '570px',
      maxWidth: '880px',
      data: {
        dialogConfirmation: this.cantidad,
        pedidodetalleer: this.pedidoDetalleSeleccionado,
        idEstablecimientoSeleccionado: this.idestablecimientoseleccionado,

      }
    });


    this.dialogRef.afterClosed().subscribe(result => {
      // console.log(result);
      if (result == true) {
        // this.personaService.cargarDatosPersonaPorDNI(this.parentForm.get('dni').value).subscribe((data: any) => {
        //   this.alumnoService.getByDni(this.parentForm.get('dni').value).subscribe((data: any) => {
        //     this.alumnoPersonaIfExisteEmmiter.emit(data.content);
        //   })
        //   // this.parentForm.patchValue(data.content);
        //   // this.parentForm.disable();

        // })




      }
    });
  }

  getproductos(idEstablecimiento: string) {
    this.loadingFlag = true;
    this.productosService.getProductosByEstablecimiento(idEstablecimiento).valueChanges().subscribe(data => {
      this.productos = data
      this.productos.forEach(p => {
        var pd = new PedidoDetalleEr();
        pd.producto = p;
        this.productsToPedidoDetalle.push(pd);
      });
      this.loadingFlag = false;
      // console.log(data)
    })
  }


}
