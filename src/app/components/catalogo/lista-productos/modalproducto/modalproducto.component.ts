import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, HostListener, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PedidosService } from 'src/app/components/services/pedidos.service';
import { PedidoDetalleEr } from 'src/app/domain/entity/pedidodetalleer';

@Component({
  selector: 'app-modalproducto',
  templateUrl: './modalproducto.component.html',
  styleUrls: ['./modalproducto.component.css']
})
export class ModalproductoComponent implements OnInit {
  hide = true;
  formPassword: FormGroup;

  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  nota:string='';

  imageLoader = true;
  constructor(
    public dialogRef: MatDialogRef<ModalproductoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private pedidoService: PedidosService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 768px)');
    this._mobileQueryListener = () => {

      changeDetectorRef.detectChanges()

      this.mobileQuery.matches ? this.dialogRef.updateSize('93%', '100%') : this.dialogRef.updateSize('880px', '570px')
    };

    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    window.innerWidth < 768 ? this.dialogRef.updateSize('93%', '100%') : this.dialogRef.updateSize('880px', '570px')
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  cerrarDialogo() {

    // this.data.dialogConfirmation=true;
    this.dialogRef.close(this.data);

  }



  eliminarTodosLosPedidos()
  {
    this.pedidoService.eliminarTodosLosPedidos();
  }

  incrementarCantidad = (pd: PedidoDetalleEr) => pd.cantidad < 100 ? pd.cantidad += 1 : pd.cantidad;

  decrementarCantidad = (pd: PedidoDetalleEr) => pd.cantidad > 1 ? pd.cantidad -= 1 : 1;

  agregarDetallePedidoEr(pd: PedidoDetalleEr) {

    let ptemp = Object.assign({}, pd)
    ptemp.nota=this.nota;
    this.pedidoService.agregarPedidoDetalleEr(ptemp, this.data.idEstablecimientoSeleccionado);
    pd.cantidad = 1;

    this.cerrarDialogo();
  }

}



export interface DialogData {
  cantidad: number;
  pedidodetalleer: PedidoDetalleEr;
  idEstablecimientoSeleccionado: string;
}

