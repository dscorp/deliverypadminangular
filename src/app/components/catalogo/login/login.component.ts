import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import firebase from 'firebase/app'
import { first } from 'rxjs/operators';
import { UserFirebase } from 'src/app/domain/entity/userfirebase';
import { AuthService } from '../../services/auth.service';
import { UsersService } from '../../services/users.service';
import { TelefonodialogComponent } from './telefonodialog/telefonodialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  provider = new firebase.auth.FacebookAuthProvider()
  fbUser = null;
  telefono;
  constructor(public afAuth: AngularFireAuth,
    public dialog: MatDialog, private router: Router,
    private zone: NgZone,
    private userService: UsersService,
    private authService: AuthService) {


  }

  ngOnInit() {


    firebase.auth().languageCode = 'es';
    this.provider.setCustomParameters({
      'display': 'popup',
    });
  }




  openDialog(userRealTime:UserFirebase): void {
    const dialogRef = this.dialog.open(TelefonodialogComponent, {
      width: '500px',
      disableClose: true,
      data: {
        telefono: '',
      }
    });

    dialogRef.afterClosed().subscribe(telefono => {


      userRealTime.telefono = telefono;

        this.userService.save(userRealTime).then(data => {
          this.router.navigate(['/catalogo'])
          this.authService.guardarUser
        }).catch(error => {

        })

      // console.log('asdasdsd');
      // //REGISTRAR EL USUARIO CON EL TELEFONO DEVUELTO
      // if (this.fbUser) {
      //   console.log('telefono', result);
      //   console.log('usuario', this.fbUser);

      //   //el usuario con su token respectivo
      //   var userFirebase = firebase.auth().currentUser;

      //   //el que es registrado en la base de datos realtime
      //   var userdelivery = new UserFirebase();
      //   userdelivery.id = userFirebase.uid;
      //   userdelivery.nombreCompleto = this.fbUser.displayName;
      //   userdelivery.tipoUsuario = 'cliente';
      //   userdelivery.telefono = result;

      //   this.userService.main(userdelivery).then(data => {
      //     this.router.navigate(['/catalogo'])
      //   }).catch(error => {

      //   })
      // }

    });
  }


  googleAuth() {
    this.router.navigate(['telefono']);
  }

  facebookAuth() {
    firebase.auth().signInWithPopup(this.provider).then((result: any) => {
      // This gives you a Facebook Access Token. You can use it to access the Facebook API.
      var token = result.credential.accessToken;

      // El usuario de facebook
      var userFacebook = result.user;
      this.fbUser = result.user;



      // this.zone.run(() => {
      //   this.openDialog()
      // });

    }).catch(function (error) {
      console.log(error);
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...
    });
  }


  fbAuth() {
    this.authService.facebookAuth().then((userRealTime:UserFirebase)=>{

      this.userService.getbyuid(userRealTime.id).valueChanges().pipe( first()).subscribe( (user:UserFirebase) =>{
        console.log('user',user);

               if(user)
                   {
                     console.log('el usuario existe');
                     this.authService.guardarUser(user);
                     this.router.navigate(['/catalogo'])
                   }
                   else{
                     console.log('el usuario no existe');
                     this.openDialog(userRealTime);
                     // this.userService.main(userRealTime)

                   }

             })
        //COMPROBAR SI EXISTE USUARIO CON ESA UID EN LA REALTIME
        // return this.usersService.getbyuid(userRealTime.id);


      // observable.valueChanges().subscribe((userRealTime:UserFirebase) =>{
      //     if(userRealTime)
      //     {
      //       console.log('el usuario existe');
      //     }
      //     else{
      //       console.log('el usuario no existe');
      //       this.openDialog(userRealTime);
      //       // this.userService.main(userRealTime)
      //     }
      // })
    })
  }

  onLogOut() {
    this.afAuth.auth.signOut();
  }
}
