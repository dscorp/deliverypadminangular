import { FormControl, Validators } from '@angular/forms';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/components/services/users.service';

@Component({
  selector: 'app-telefonodialog',
  templateUrl: './telefonodialog.component.html',
  styleUrls: ['./telefonodialog.component.css']
})
export class TelefonodialogComponent implements OnInit {


  fcontroltelefono = new FormControl([''],[Validators.required,Validators.maxLength(9),Validators.minLength(9)]);

  constructor(
    public dialogRef: MatDialogRef<TelefonodialogComponent>,
     private router:Router,
    @Inject(MAT_DIALOG_DATA) public data: DialogDatatlf)
     {

     }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

onBtnActionOK()
{
  console.log(this.fcontroltelefono);
  if(this.fcontroltelefono.valid)
  {
    this.dialogRef.close(`+51${this.data.telefono}`);
  }

}

}

export interface DialogDatatlf {
  telefono:string;
}

