import { Component, ElementRef, HostListener, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { element } from 'protractor';
import { Pedido } from 'src/app/domain/entity/pedido';
import { PedidoDetalle } from 'src/app/domain/entity/pedidodetalle';
import { AuthService } from '../../services/auth.service';
import { PedidodetalleService } from '../../services/pedidodetalle.service';
import { PedidosService } from '../../services/pedidos.service';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],

})
export class NavbarComponent implements OnInit {


  pedidos:Array<Pedido>;

  usermenuclicked:boolean= false;
  @Input() sidenav:MatSidenav;

  @ViewChild('menuuseropt',null)  menuuseropt:HTMLElement
  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
      if (!targetElement) {
          return;
      }
      const clickedInside = this.elRef.nativeElement.contains(targetElement);
      if (!clickedInside) {
          this.usermenuclicked=false;
      }
  }
//   pedidoskat =[
//     "-MKzveLKPk9L-KvBBTBi",
//     "-ML_K7HvAKHvnm03tcTV",
//     "-MLAk0A2vVC1r5aIuss3",
//     "-MLefJkOk4XLTwz6KAhp",
//     "-MLFHZRIW_xGL4lG0jwR",
//     "-MLFlHNKhedYZdYMtZQ5",
//     "-MLKcFTDDAV--8UKoswI",
//     "-MLL31GtKMpSh5I2nit6",
//     "-MLLT3vyOFbWkLdWrBi4",
//     "-MLuIL-SkS-enVZ9PS5_",
//     "-MLUlS32sVr-lpLwPRkZ",
//     "-MLVH8EhZCXQCaUXeoJM",
//     "-MLyrW9QZslT2GUmjfcR",
//     "-MM8W417-ad3EmUIrsQt",
//     "-MMDobeJfdGmOYoz9mRI",
//     "-MMIikwFgjTCaR2OmfnG",
//     "-MMn03_XUKy2o2BvcB_6",
//     "-MMNu7UNTKEcN_29cQtz",
//     "-MMwyy4c4HZpPH-ivHJD",
//     "-MMx9zqYfYKAxMm2bdmS",
//     "-MMYrUvByCZxYoVgpcB4",
//     "-MNB2PaSdzA209Ial9Y3",
//     "-MNB3rLoI8vQLjM-4sEi",
//     "-MNBLPKrxF4sR0IBuFj5",
//     "-MNBP6WTK47ZUHwSubwi",
//     "-MNLndmWCYNdfidZssHF",
//     "-MNLTEssV6u39WaFNVFf",
//     "-MNQfed0nYeKL1P9GwT4"
//  ]


  detalleskat: any[];


  constructor(public authservide:AuthService,
              public pedidoService:PedidosService,
              private elRef: ElementRef,
              private renderer: Renderer2)
   {

    }

  ngOnInit() {
    // this.pedidoService.gettotal().valueChanges().subscribe(data=>{
    //   this.pedidos=data;
    //   var subtotal:number=0;
    //   this.pedidos.forEach(p =>{
    //     console.log(p.subtotal);
    //     try {
    //      var valor:number = p.subtotal;
    //      subtotal = +subtotal+  +p.subtotal;
    //      console.log('esto es sumado');
    //      console.log(subtotal);
    //     } catch (error) {

    //     }

    //   })
    //   console.log('este es');
    //   console.log(subtotal);
    // })

  //  this.getproductosvendidos();
  // this.getallUsers();
  }

// getallUsers()
// {
// this.usersService.getallusers().valueChanges().subscribe(data=>{
//   console.log(JSON.stringify(data));
// })
// }

// getproductosvendidos()
// {


// this.detalleskat=new Array();

// this.pedidoskat.forEach((element:string) => {

//   this.pedidoDetalleService.getByPedido(element).valueChanges().subscribe((pd)=>{

//     pd.forEach((element3) => {
//           this.detalleskat.push(element3);
//     });


//     console.log(JSON.stringify(this.detalleskat));

//   })

// });







// }


onbtnusermenuclicked()
{
  this.usermenuclicked=!this.usermenuclicked;
}
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
  }


}
