import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Establecimiento } from 'src/app/domain/entity/establecimiento';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EstablecimientoService {

  path = "https://delivery-p.firebaseio.com/Establecimientos.json";
  establecimientoList: AngularFireList<Establecimiento>;



  constructor(private database: AngularFireDatabase,
    private http: HttpClient
  ) { }


  ordenarPorPrioridad = (a, b) => {
    if (a.prioridad < b.prioridad)
      return -1;
    if (a.prioridad > b.prioridad)
      return 1;
    return 0;
  };

  getEstablecimientos() {
    return this.establecimientoList = this.database.list<Establecimiento>('Establecimientos');
  }

  mycomparator(a,b) {
    return parseInt(a.prioridad, 10) - parseInt(b.prioridad, 10);
  }

  getestablecimientosApi() {
    return this.database.list<Establecimiento>('Establecimientos').valueChanges().pipe( map((data:[])=>data.sort(this.mycomparator)));
  }

  // establecimeinto por  Id
  getEstablecimeintoById(idEstablecimiento: string) {
    return this.establecimientoList = this.database.list<Establecimiento>('Establecimeinto/' + idEstablecimiento);
  }

  getById(id: string) {
    return this.database.object<Establecimiento>('Establecimientos/' + id);
  }

  create(establecimiento: Establecimiento) {
    establecimiento.categoriaEstablecimiento_nombreEstablecimiento = establecimiento.categoriaEstablecimiento + "_" + establecimiento.nombre;
    this.database.object('Establecimientos/' + establecimiento.idUsers).set(establecimiento);
  }

  // checkIfUsersIdExistOnEstablecimiento(idUsers:string)
  // {
  //   return this.database.list("Establecimientos",query=>query.orderByChild('id').equalTo(idUsers));
  // }

  checkIfUsersIdExistOnEstablecimiento(idUsers: string): Observable<Establecimiento> {
    return this.database.object<Establecimiento>("Establecimientos/" + idUsers).valueChanges();
  }


  fixHorarios() {
    this.database.list("Establecimientos").valueChanges().subscribe((establecimientos:any)=>{

      const horario ={
        haLunes:12,
        hcLunes:22,
        haMartes:12,
        hcMartes:22,
        haMiercoles:12,
        hcMiercoles:22,
        haJueves:12,
        hcJueves:22,
        haViernes:12,
        hcViernes:22,
        haSabado:12,
        hcSabado:22,
        haDomingo:12,
        hcDomingo:22,
        horaAperturaDiaNormal:12,
        horaCierreDiaNormal:22,
        horaAperturaSabados:12,
        horaCierreSabados:22,
        horaAperturaDomingo:12,
        horaCierreDomingo:22,

      }


      for (let establecimiento of establecimientos) {
          this.database.object("Establecimientos/"+establecimiento.id).update({horario:horario}).then()
      }


    })
  }
}
