import { Component, OnInit } from '@angular/core';
import { EstablecimientoService } from '../../establecimiento.service';
import { Establecimiento } from 'src/app/domain/entity/establecimiento';
import { MatTableDataSource } from '@angular/material/table';
import {ListaproductosService} from '../../../../services/forParameters/listaproductos.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(private establecimientoService: EstablecimientoService,
    private lproductosService:ListaproductosService,
             private router:Router
  )
  { }

  displayedColumns: string[] = ['nombre', 'descripcion', 'direccion', 'action'];

  dataSource: MatTableDataSource<Establecimiento>;



  routelistarProductos(establecimiento:any)
  {
    this.lproductosService.establecimientoSelected=establecimiento
    this.router.navigate(['listProductsByEstab'])
  }

  routelistarPromociones(establecimiento:any)
  {
    this.lproductosService.establecimientoSelected=establecimiento
    this.router.navigate(['listPromossByEstab'])
  }



  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  ngOnInit() {
    this.getEstablecimientos();
  }

  getEstablecimientos() {
    this.establecimientoService.getEstablecimientos().valueChanges().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
    });

  }




}
