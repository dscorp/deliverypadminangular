import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogData} from '../../../../../catalogo/lista-productos/modalproducto/modalproducto.component';
import {MatTableDataSource} from '@angular/material/table';
import {ProductoService} from '../../../../../services/producto.service';
import {ListaproductosService} from '../../../../../services/forParameters/listaproductos.service';
import {Producto} from '../../../../../../domain/entity/producto';
import {Promocion} from '../../../../../../domain/entity/Promocion';
import {AngularFireDatabase} from '@angular/fire/database';
import Swal from 'sweetalert2';
import {PromocionesService} from '../../../../../services/promociones.service';

@Component({
  selector: 'app-dialog-registrar-promocion',
  templateUrl: './dialog-registrar-promocion.component.html',
  styleUrls: ['./dialog-registrar-promocion.component.css']
})
export class DialogRegistrarPromocionComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'descripcion', 'precio','accion'];
  dataSource: MatTableDataSource<any>;

  productoSeleccionado:Producto = new Producto();
  validezdias:number
  constructor(
    public dialogRef: MatDialogRef<DialogRegistrarPromocionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private productoService: ProductoService,
    private lproductoService: ListaproductosService,
    private db:AngularFireDatabase,
    private promocionService:PromocionesService,
  ) {
    this.listarProductos();
  }


  ngOnInit() {
  }


  seleccionarProducto(producto:Producto)
  {
    this.productoSeleccionado=producto
    console.log('productoselec',this.productoSeleccionado);
  }

  registrarPromocion()
  {

    if(this.productoSeleccionado.id===undefined)
    {
        Swal.fire("Debes seleccionar un producto","","info")
      return
    }
    if (this.validezdias==null)
    {
      Swal.fire("especificar una validez en dias","","info")
      return
    }

    const promocion = new Promocion()
    promocion.id=this.db.createPushId();
    promocion.fechaCreacion= new Date().getTime()

    promocion.idProducto=this.productoSeleccionado.id
    promocion.idEstablecimiento=this.productoSeleccionado.idEstablecimiento
    promocion.imageurl=this.productoSeleccionado.imagen
    promocion.nombreEstablecimiento=this.productoSeleccionado.nombreEstablecimiento

//DATOS DE PRODUCTOS
    promocion.imagenProducto=this.productoSeleccionado.imagen
    promocion.logoEstablecimiento=this.productoSeleccionado.logoEstablecimiento
    promocion.nombreProducto=this.productoSeleccionado.nombre
    promocion.descripcion=this.productoSeleccionado.descripcion
    promocion.precioProducto=this.productoSeleccionado.precio
    promocion.validezEnDias=7

    this.promocionService.save(promocion)
      .then(it=>{
        Swal.fire("Promocion registrada exitosamente","","success")
        this.dialogRef.close()
      })
      .catch(err=>{
        Swal.fire("No se pudo registrar la promocion",err.message,"error")
        this.dialogRef.close()
      });
  }


  listarProductos() {
    this.productoService.getProductosByEstablecimientoV2(this.lproductoService.establecimientoSelected.id)
      .valueChanges().subscribe(productos => {
      this.dataSource = new MatTableDataSource<any>(productos);
    });
  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
