import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ListaproductosService} from '../../../../services/forParameters/listaproductos.service';
import {ProductoService} from '../../../../services/producto.service';
import {PromocionesService} from '../../../../services/promociones.service';
import Swal from 'sweetalert2';
import {Producto} from '../../../../../domain/entity/producto';
import {MatDialog} from '@angular/material';
import {DialogRegistrarPromocionComponent} from './dialog-registrar-promocion/dialog-registrar-promocion.component';

@Component({
  selector: 'app-listar-produc  tos',
  templateUrl: './listar-promociones.component.html',
  styleUrls: ['./listar-promociones.component.css']
})
export class ListarPromocionesComponent implements OnInit {
  displayedColumns: string[] = ['nombreProducto', 'descripcion', 'precioProducto', 'action'];

  dataSource: MatTableDataSource<any>;

  constructor(
    private lpservice: ListaproductosService,
    private productoService: ProductoService,
    private promocionesService: PromocionesService,
    public dialog: MatDialog,
  ) {

  }


  openDialog(): void {
    const dialogRef = this.dialog.open(DialogRegistrarPromocionComponent, {
      width: '1200px',
      height:'800px',
      data: {establecimiento: this.lpservice.establecimientoSelected}
    });
  }


  // //
  // agergarAPromociones(producto: Producto) {
  //
  //   this.promocionesService.save(producto)
  //     .then(it => {
  //       Swal.fire('Promoción registrada exitosamente', '', 'success');
  //     })
  //     .catch(err => {
  //       Swal.fire('No registró la promocion', err.message, 'error');
  //     });
  // }

  listarPromociones() {


    this.promocionesService.get(this.lpservice.establecimientoSelected.id)
      .valueChanges().subscribe(promociones => {
      this.dataSource = new MatTableDataSource(promociones);
    });
  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    this.listarPromociones();
  }

}
