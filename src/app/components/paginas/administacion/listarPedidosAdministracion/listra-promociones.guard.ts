import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {ListaproductosService} from '../../../services/forParameters/listaproductos.service';
import {Location} from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class ListraPromocionesGuard implements CanActivate {

  constructor(private lProductosService:ListaproductosService, private location:Location) {
  }
  canActivate(): boolean
  {
    if(this.lProductosService.establecimientoSelected!=null)
      return true
    else{
      this.location.back();
      return false;
    }
  }

}

