import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogModificarProductoComponent } from './dialog-modificar-producto.component';

describe('DialogModificarProductoComponent', () => {
  let component: DialogModificarProductoComponent;
  let fixture: ComponentFixture<DialogModificarProductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogModificarProductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogModificarProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
