import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogData} from '../../../../catalogo/lista-productos/modalproducto/modalproducto.component';
import {DialogRegistrarPromocionComponent} from '../../listarPedidosAdministracion/listar-promociones/dialog-registrar-promocion/dialog-registrar-promocion.component';
import {ListaproductosService} from '../../../../services/forParameters/listaproductos.service';
import {UploadService} from '../../../../services/upload.service';
import {ProductoService} from '../../../../services/producto.service';
import {NgForm} from '@angular/forms';
import ImageResize from 'image-resize';
import firebase from 'firebase/app';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-modificar-producto',
  templateUrl: './dialog-modificar-producto.component.html',
  styleUrls: ['./dialog-modificar-producto.component.css']
})
export class DialogModificarProductoComponent implements OnInit {
  private fileToUpload: any;
  imageResize = new ImageResize();

  file: any = null;

  constructor(public dialogRef: MatDialogRef<DialogRegistrarPromocionComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              public lproductoService: ListaproductosService,
              private uploadService: UploadService,
              private productoService: ProductoService) {

  }

  actualizarProducto(form: NgForm) {


    if (form.valid) {
      const nuevaData = form.value;
      if (this.fileToUpload == undefined) {

        const prodSelected = this.lproductoService.productoSeleccionado;
        prodSelected.nombre = nuevaData.nombre;
        prodSelected.precio = nuevaData.precio;
        prodSelected.disponible = nuevaData.disponible;
        prodSelected.categoria = nuevaData.categoria;
        prodSelected.descripcion = nuevaData.descripcion;
        prodSelected.prioridad = nuevaData.prioridad;
        this.productoService.update(prodSelected)
          .then(()=>{
            Swal.fire('Producto actualizado exitosamente','','success')
            this.dialogRef.close()
          })
          .catch(e=>{
            Swal.fire('No se pudo actualizar el producto',e.message,'error')

          })

      } else {
        console.log('formulario valido con actualizacion de imagen');

        if (this._esImagen(this.fileToUpload.type)) {


          const uploadTask: firebase.storage.UploadTask = this.uploadService.uploadProductoImage(this.fileToUpload);

          uploadTask.on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot: firebase.storage.UploadTaskSnapshot) => {
              console.log('subiendo');
            }
            ,

            (error) => {
              console.error('Error al subir', error);
            },

            () => {

              this.uploadService.getImageUrl(this.fileToUpload.name)
                .then(ImageUrl=>{

                  console.log('Imagen cargada correctamente');
                  const lastImageUrl = this.lproductoService.productoSeleccionado.imagen;
                  const prodSelected = this.lproductoService.productoSeleccionado;
                  prodSelected.nombre = nuevaData.nombre;
                  prodSelected.precio = nuevaData.precio;
                  prodSelected.imagen = ImageUrl;
                  prodSelected.disponible = nuevaData.disponible;
                  prodSelected.categoria = nuevaData.categoria;
                  prodSelected.descripcion = nuevaData.descripcion;
                  prodSelected.prioridad = nuevaData.prioridad;
                  console.log(ImageUrl);
                  console.log(prodSelected);
                  this.productoService.update(prodSelected)
                    .then(()=>{
                    Swal.fire('Producto actualizado exitosamente','','success')
                      this.uploadService.eliminarImagenByUrl(lastImageUrl)
                      this.dialogRef.close()
                  })
                    .catch(e=>{
                      Swal.fire('No se pudo actualizar el producto',e.message,'error')

                    })
                })

            }
          );


        }


      }
    } else {
      console.log('form no valido');
    }
  }


  handleFileInput() {

    return this.imageResize
      .updateOptions({
        width: 700,
        format: 'jpg',
        quality: 0.5,
        outputType: 'blob'
      })
      .play(document.getElementById('upload'))
      .then((response) => {
        //CONVERTIR BLOB A FILE
        const imageAsFile = new File([response], new Date().getTime().toString() + '.jpeg', {type: 'image/jpeg'});
        this.fileToUpload = imageAsFile;
      })
      .catch(function(error) {
        console.error(error);
      });

  }


  ngOnInit() {
  }

  private _esImagen(tipoArchivo: string): boolean {
    return (tipoArchivo === '' || tipoArchivo === undefined) ? false : tipoArchivo.startsWith('image');
  }
}
