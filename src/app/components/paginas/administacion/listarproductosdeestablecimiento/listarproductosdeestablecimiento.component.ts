import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {Producto} from 'src/app/domain/entity/producto';
import {ProductoService} from 'src/app/components/services/producto.service';
import {CategoriasPropiasestabService} from 'src/app/components/services/categorias-propiasestab.service';
import {Observable} from 'rxjs';
import {CategoriaPropiaEstablecimiento} from 'src/app/domain/entity/categoriaPropiaEstablecimiento';
import {DialogRegistrarPromocionComponent} from '../listarPedidosAdministracion/listar-promociones/dialog-registrar-promocion/dialog-registrar-promocion.component';
import {MatDialog} from '@angular/material';
import {ListaproductosService} from '../../../services/forParameters/listaproductos.service';
import {DialogModificarProductoComponent} from './dialog-modificar-producto/dialog-modificar-producto.component';

@Component({
  selector: 'app-listarproductosdeestablecimiento',
  templateUrl: './listarproductosdeestablecimiento.component.html',
  styleUrls: ['./listarproductosdeestablecimiento.component.css']
})
export class ListarproductosdeestablecimientoComponent implements OnInit {


  idEstablecimiento: string;
  dataSource: MatTableDataSource<Producto>;
  displayedColumns: string[] = ['nombre', 'descripcion', 'precio', 'action'];
  categoriasList: Observable<CategoriaPropiaEstablecimiento[]>;


  //FLAGS
  loadingFlag = false;


  constructor(
    private _activatedRoute: ActivatedRoute,
    private productoService: ProductoService,
    private categoriaPropiaEstService: CategoriasPropiasestabService,
    public dialog: MatDialog,
    private lpservice: ListaproductosService,
  ) {


    // this._activatedRoute.params.subscribe(params => {
    //   this.idEstablecimiento = params['idestablecimiento'];
    //   if (this.idEstablecimiento == null) {
    //     // console.log('no se esta reciebiendo el id del establecimiento');
    //   } else {
    //
    //     // console.log('este es el id de establecimiento' +this.idEstablecimiento);
    //   }
    // });


  }


  openDialogModificarProducto(producto:Producto): void {

    this.lpservice.productoSeleccionado=producto;

    const dialogRef = this.dialog.open(DialogModificarProductoComponent, {
      width: '1000px',
      height: '800px',

    });
  }

  getCategoriasByEstablecimiento() {
    this.categoriasList = this.categoriaPropiaEstService.getByEstablecimiento(this.idEstablecimiento).valueChanges();
  }

  getProductoByEstablecimiento(idEstablecimiento: string) {
    this.loadingFlag = true;
    this.productoService.getProductosByEstablecimientoV2(this.lpservice.establecimientoSelected.id).
    valueChanges().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.loadingFlag = false;
    });

  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    this.getProductoByEstablecimiento(this.idEstablecimiento);
    this.getCategoriasByEstablecimiento();
  }

}
