import {PedidodetalleService} from '../../../../services/pedidodetalle.service';
import {PedidosService} from '../../../../services/pedidos.service';
import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: "app-informemensual",
  templateUrl: "./informemensual.component.html",
  styleUrls: ["./informemensual.component.css"],
})
export class InformemensualComponent implements OnInit {
  constructor(
    private pedidosService: PedidosService,
    private pedidoDetalleService: PedidodetalleService
  ) {}

  productosparseados: any = {};
  productosmap = new Map<string, string>();

  ngOnInit() {}

  ngAfterViewInit() {
    // ...
    // this.generarReporte();
    // this.transformar()
  }

  fecheInicio1: any;
  fechaFin1: any;

  pedidosmain = new Array<any>();
  detallesmain = new Array<any>();
  datafinal = new Array<any>();
  dataAgrupada = new Array<any>();
  dataGraficos = new Array<any>();
  dataAgrupadaAsArray = new Array<any>();


  fecheInicioParseada: any;
  fechaFinParseada: any;
  //
  // transformar() {
  //   // console.log("transformando");
  //   var productos = new Array<any>();
  //
  //   //IMOPRATA EL JSON DESDE UN ARCHIVO
  //   var data = require("./productos.json");
  //
  //   var listaEstablecimientos = require("./establecimientos.json");
  //
  //
  //   Object.keys(data).forEach((key: any) => {
  //     var establecimiento = data[key];
  //
  //     Object.keys(establecimiento).forEach((key2) => {
  //       establecimiento[key2].idEstablecimiento = key;
  //
  //       var producto = establecimiento[key2];
  //
  //       if(listaEstablecimientos[producto.idEstablecimiento]!==undefined)
  //       {
  //
  //         var establecimientosss = listaEstablecimientos[producto.idEstablecimiento];
  //
  //         var estab = {
  //           [key2]: {
  //             categoria: producto.categoria,
  //             categoria_Nombre: producto.categoria_Nombre,
  //             descripcion: producto.descripcion,
  //             disponible: producto.disponible,
  //             id: producto.id,
  //             imagen: producto.imagen,
  //             nombre: producto.nombre,
  //             precio: producto.precio,
  //             prioridad: producto.prioridad,
  //             idEstablecimiento: producto.idEstablecimiento,
  //             nombreEstablecimiento: establecimientosss.nombre,
  //             logoEstablecimiento:establecimientosss.logo
  //
  //           },
  //         };
  //
  //         this.productosparseados = {
  //           ...this.productosparseados,
  //           ...estab,
  //         };
  //       }
  //
  //     });
  //   });
  //
  //   // console.log('mostando data');
  //   // console.log(JSON.stringify(this.productosparseados))
  //
  //
  // }

  onSubmit(form: NgForm) {
    if (form.valid) {
      const fechainicial: number = (form.value.fechaInicio as Date).getTime();
      const fechafinal: number = (form.value.fechaFin as Date).getTime();
      this.pedidosmain = new Array<any>();
      this.detallesmain = new Array<any>();
      this.datafinal = new Array<any>();
      this.dataAgrupada = new Array<any>();
      this.dataGraficos = new Array<any>();;
      this.dataAgrupadaAsArray = new Array<any>();
      this.generarReporte(fechainicial, fechafinal);
    }
  }
  generarReporte(fecheInicio: number, fechaFin: number) {
    //IMOPRATA EL JSON DESDE UN ARCHIVO
    this.fecheInicioParseada=fecheInicio;
    this.fechaFinParseada=fechaFin
    var data = require("./data.json");

    var pedidos = data.Pedido;
    var detalles = data.PedidoDetalle;
    var establecimientos = data.Establecimientos;

    //CREA  UN UNICO OBJETO A PARTIR DE PEDIDODETALLES Y PEDIDOS
    Object.keys(detalles).forEach((key) => {
      var temp = detalles[key];
      Object.keys(temp).forEach((element) => {
        // console.log(temp[element]);
        var newmodel = {
          ...temp[element],
          ...pedidos[key],

          valorventa: temp[element].cantidad * temp[element].precioVenta,
        };
        this.detallesmain.push(newmodel);
      });
    });

    // console.log(this.detallesmain);

    //FILTRA LA INFORMACION   Y EXTRAE LOS REGISTROS QUE SE ENCUENTREN DENTRO DEL INTERVALO DE FECHAS ESTABLECIDAS  Y CON ESTADO FINALIZADO
    this.detallesmain.forEach((detallePedidoConFecha) => {

      if (detallePedidoConFecha.estado == "finalizado") {
        // console.log('cumple '+ detallePedidoConFecha.estado);

        let fechadetalle = new Date(detallePedidoConFecha.created_at).getTime();

        if (fechadetalle > fecheInicio && fechadetalle < fechaFin) {
          this.datafinal.push(detallePedidoConFecha);
        }
      }
    });



    //CONVIERTE LA DATA A FORMATO PARA GRAFICOS ESTADISTICOS DANDOLE EL  LOS ATRIBUTOS ETABLECIMIENTO CON EL NOMBRE Y DATA CON LA LISTA DE PRODUCTOS VENDIDOS
    Object.keys(establecimientos).forEach((key) => {
      var establecimiento = establecimientos[key];
      var name = establecimiento.nombre.replace(/\s/g, "");
      name = name.replace(/,/g, "");
      name = name.replace(/'/g, "");
      name = name.replace(/-/g, "");
      name = name.replace(/\./g, "");

      const result = this.datafinal.filter(
        (detalleFiltrado) =>
          detalleFiltrado.nombreEstablecimiento === establecimiento.nombre
      );

      var info = {
        establecimiento: name,
        data: result,
      };
      this.dataAgrupada.push(info);
    });




    // ORDENA POR EL NUMERO DE VENTAS
    this.dataAgrupada.sort((a, b) => (a.data.length < b.data.length ? 1 : -1));

    //aqui generar datos para agrupar de acuerdo al grafico  e informacion que se quiere mostrar

    //AGRUPA POR NOMBRE DE PRODUCTO Y SUMA LAS CANTIDADES
    // console.log('data agrupada',this.dataAgrupada);





    this.dataAgrupada.forEach((element) => {

      console.log(this.dataAgrupada)

      var sortedData = this.groupByNombre(element.data);

      //ORDENA POR CANTIDADES
      sortedData.sort((a, b) => (a.cantidad < b.cantidad ? 1 : -1));
      this.dataGraficos = {
        ...this.dataGraficos,
        [element.establecimiento]: {
          dataAgrupadaPorProducto: sortedData,
          detallesPedido:element.data
        },
      };
    });

    //AGREGAR EL SUBTOTAL DE VENTAS

    this.dataAgrupada.forEach((element) => {
      var contador = 0;
      var totalventas = 0;
      element.data.forEach((detalle) => {
        totalventas += detalle.valorventa;
        contador += 1;
      });

      this.dataGraficos[element.establecimiento] = {
        ...this.dataGraficos[element.establecimiento],
        subtotal: totalventas,
        productosVendidos: contador,
      };
    });

    //agrupa por lugar
    this.dataAgrupada.forEach((element) => {
      var sortedData = this.groupBylugar(element.data);

      sortedData.sort((a, b) => (a.cantidad < b.cantidad ? 1 : -1));

      this.dataGraficos[element.establecimiento] = {
        ...this.dataGraficos[element.establecimiento],
        dataAgrupadaPorLugar: sortedData,
      };
    });

    // console.log(this.dataGraficos)

    // this.dataAgrupada.forEach(element => {
    //   element.created_at = new Date(element.created_at).getDay();
    // });

    var days = [
      "Domingo",
      "Lunes",
      "Martes",
      "Miercoles",
      "Jueves",
      "Viernes",
      "Sabado",
    ];

    this.dataAgrupada.forEach((element) => {
      element.data.map((detalle) => {
        var dia = days[new Date(detalle.created_at).getDay()];
        detalle.created_at = dia;
        return detalle;
      });

      // console.log(element.data);
      var sortedData = this.groupByDiaDeLaSemana(element.data);
      sortedData.sort((a, b) => (a.cantidad < b.cantidad ? 1 : -1));

      this.dataGraficos[element.establecimiento] = {
        ...this.dataGraficos[element.establecimiento],
        dataAgrupadaPorDiaSemana: sortedData,
      };
    });

    console.log("graficos",this.dataGraficos)

    Object.keys(this.dataGraficos).forEach((key) => {
      this.dataAgrupadaAsArray.push({
        establecimiento: key,
        ...this.dataGraficos[key],
      });
    });

    console.log("dataasarray");
    console.log(this.dataAgrupadaAsArray);
  }

  print() {
    const printContent = document.getElementById("contenedor");
    const WindowPrt = window.open(
      "",
      "",
      "left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0"
    );
    WindowPrt.document.write(printContent.innerHTML);
    WindowPrt.document.close();
    WindowPrt.focus();
    WindowPrt.print();
    // WindowPrt.close();
  }

  groupByNombre(data) {
    return data.reduce((acc, { nombreProducto, cantidad }) => {
      const item = acc.find((el) => el.nombreProducto === nombreProducto);

      if (item) item.cantidad += cantidad;
      else acc.push({ nombreProducto, cantidad });

      return acc;
    }, []);
  }

  groupBylugar(data) {
    return data.reduce((acc, { lugarEntrega, cantidad }) => {
      const item = acc.find((el) => el.lugarEntrega === lugarEntrega);

      if (item) item.cantidad += 1;
      else acc.push({ lugarEntrega, cantidad });

      return acc;
    }, []);
  }

  groupByDiaDeLaSemana(data) {
    return data.reduce((acc, { created_at, cantidad }) => {
      // console.log('esto es acc');
      // console.log(acc);
      const item = acc.find((el) => el.created_at === created_at);

      if (item) item.cantidad += 1;
      else acc.push({ created_at, cantidad });
      // else acc.push({
      //   "dia":date,
      //   "cantidad":cantidad,
      // });
      return acc;
    }, []);
  }
}
