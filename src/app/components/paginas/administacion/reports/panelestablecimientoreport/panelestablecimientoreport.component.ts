import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import * as d3 from 'd3';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import 'jspdf-autotable';

import {DatePipe, DecimalPipe} from '@angular/common';

@Component({
  selector: 'app-panelestablecimientoreport',
  templateUrl: './panelestablecimientoreport.component.html',
  styleUrls: ['./panelestablecimientoreport.component.css']
})
export class PanelestablecimientoreportComponent implements OnInit {

  @Input("data")
  data: any

  @Input("fechaInicio")
  fechaInicio: number

  @Input("fechaFin")
  fechaFin: number

  @ViewChild('report', null) report: ElementRef;

  private svg;
  private margin = 130;
  private width = 1000 - (this.margin * 2);
  private height = 580 - (this.margin * 2);

  constructor(public datepipe: DatePipe, public decimalPipe: DecimalPipe) {
  }

  private graphtitlefontsize = "30px"
  private graphXaxisFontSize = "18px"

  ngOnInit() {
  }

  ngAfterViewInit() {
    // ...
    if (this.data.establecimiento) {
      // console.log(this.data.establecimiento);
      this.crearGraficoProductoMasVendid(this.data.dataAgrupadaPorProducto)
      this.crearGraficoProductoDiaSemana(this.data.dataAgrupadaPorDiaSemana)
      this.crearGraficolugarventas(this.data.dataAgrupadaPorLugar);

      // dataAgrupadaPorDiaSemana
    }

  }


  imprimirReporteGeneral() {


    html2canvas(this.report.nativeElement, {scrollY: -window.scrollY}).then((canvas) => {

      var img = canvas.toDataURL("image/PNG");
      var doc = new jsPDF('l', 'pt', 'a4');
      const bufferX = 40;
      const bufferY = 40;
      const imgProps = (<any>doc).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');

      // this.report.nativeElement.appendChild(canvas)

      return doc;

    }).then((doc) => {
      doc.save(this.data.establecimiento + 'reporteGeneral.pdf');
    });

  }

  imprimirDetallesPedido() {

    console.log(this.data)
    const detallesPedido = this.data.detallesPedido.sort((a, b) => (((a.fecha) as number) > ((b.fecha) as number) ? 1 : -1))

    const doc = new jsPDF();

    const arr: any[] = [];

      detallesPedido.forEach((detalle: any) => {
        // let latest_date =this.datepipe.transform(detalle.fecha).toString();
        arr.push([
          detalle.nombreProducto.toString(),
          detalle.cantidad,
          this.decimalPipe.transform(detalle.precioVenta, '1.1-1'),
          this.decimalPipe.transform(detalle.precioVenta * detalle.cantidad, '1.1-1'),
          detalle.lugarEntrega,
        ])
      });


    var img = new Image()
    img.src = 'assets/img/logo-para-reporte.jpg'
    doc.addImage(img, 'jpg', 158, 3, 40, 27)

    const fechaFinParseada = new Date(this.fechaFin);
    const fechafin = fechaFinParseada.setDate(fechaFinParseada.getDate() - 1);

    doc.text('DETALLES DE VENTAS:\b\b' + this.datepipe.transform(this.fechaInicio) + ' - ' + this.datepipe.transform(fechafin), 15, 15);
    doc.text('ESTABLECIMIENTO: ' + this.data.establecimiento, 15, 25);
    doc.autoTable({
      head: [['PRODUCTO', 'CANTIDAD', 'PRECIO VENTA', 'SUBTOTAL', 'LUGAR DE ENTREGA']],
      body: arr,
      margin: {top: 30}

    });
    doc.save(this.data.establecimiento + 'reporteDetallado.pdf');

  }


  crearGraficolugarventas(data) {


    //GENERA EL SVG LALOUT EN EL QUE LUEGO SE PINTARA EL GRAFICO

    var string = "figure#" + this.data.establecimiento + "graficolugarventas";

    this.svg = d3.select(string)
      .append("svg")
      .attr("width", this.width + (this.margin * 2))
      .attr("height", this.height + (this.margin * 2))
      .append("g")
      .attr("transform", "translate(" + this.margin + "," + 50 + ")");


    //DIBUJA EL GRAFICO CON LOS DATOS EN EL LAYOUT


    // Create the X-axis band scale
    const x = d3.scaleBand()
      .range([0, this.width])
      .domain(data.map(d => d.lugarEntrega))
      .padding(0.2);

    // Draw the X-axis on the DOM
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end").style("font-size", this.graphXaxisFontSize);

    this.svg.append("text")
      .attr("text-anchor", "middle")
      .style("font-size", "16px")
      .style("color", "#333333")
      .attr("transform", "translate(" + (170) + "," + (-20) + ")")
      .text("Distribución de clientes.")
      .style("font-family", "Arial").style("font-size", this.graphtitlefontsize);
    // Create the Y-axis band scale
    // console.log(data);
    const y = d3.scaleLinear()

      .domain([0, data[0] ? data[0].cantidad : 1])
      .range([this.height, 0]);

    // Draw the Y-axis on the DOM
    this.svg.append("g")
      .call(d3.axisLeft(y));

    // Create and fill the bars
    this.svg.selectAll("bars")
      .data(data)
      .enter()
      .append('rect')
      .attr('x', d => x(d.lugarEntrega))
      .attr("y", d => y(d.cantidad))
      .attr("width", x.bandwidth())
      .attr("height", (d) => this.height - y(d.cantidad))
      .attr("fill", "#498bfc");
  }


  crearGraficoProductoDiaSemana(data) {
    // console.log('xxx');
    // console.log(data);

    //GENERA EL SVG LALOUT EN EL QUE LUEGO SE PINTARA EL GRAFICO

    var string = "figure#" + this.data.establecimiento + "graficodiasemana";

    this.svg = d3.select(string)
      .append("svg")
      .attr("width", this.width + (this.margin * 2))
      .attr("height", this.height + (this.margin * 2))
      .append("g")
      .attr("transform", "translate(" + this.margin + "," + 50 + ")");

    this.svg.append("text")
      .attr("text-anchor", "middle")
      .style("font-size", this.graphtitlefontsize)
      .style("color", "#333333")
      .attr("transform", "translate(" + (310) + "," + (-20) + ")")
      .text("Que día vende más y que día vende menos.")
      .style("font-family", "Arial");

    //DIBUJA EL GRAFICO CON LOS DATOS EN EL LAYOUT


    // Create the X-axis band scale
    const x = d3.scaleBand()
      .range([0, this.width])
      .domain(data.map(d => d.created_at))
      .padding(0.2);

    // Draw the X-axis on the DOM
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end").style("text-anchor", "end").style("font-size", this.graphXaxisFontSize)

    // Create the Y-axis band scale
    // console.log(data);
    const y = d3.scaleLinear()

      .domain([0, data[0] ? data[0].cantidad : 1])
      .range([this.height, 0]);

    // Draw the Y-axis on the DOM
    this.svg.append("g")
      .call(d3.axisLeft(y));

    // Create and fill the bars
    this.svg.selectAll("bars")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", d => x(d.created_at))
      .attr("y", d => y(d.cantidad))
      .attr("width", x.bandwidth())
      .attr("height", (d) => this.height - y(d.cantidad))
      .attr("fill", "#498bfc");
  }


  crearGraficoProductoMasVendid(data) {


    //GENERA EL SVG LALOUT EN EL QUE LUEGO SE PINTARA EL GRAFICO

    var string = "figure#" + this.data.establecimiento;

    this.svg = d3.select(string)
      .append("svg")
      .attr("width", this.width + (this.margin * 2))
      .attr("height", this.height + (this.margin * 2))
      .append("g")
      .attr("transform", "translate(" + this.margin + "," + 50 + ")");

    this.svg.append("text")
      .attr("text-anchor", "middle")
      .style("font-size", this.graphtitlefontsize)
      .style("color", "#333333")
      .attr("transform", "translate(" + (170) + "," + (-20) + ")")
      .text("Productos más vendidos.")
      .style("font-family", "Arial");

    //DIBUJA EL GRAFICO CON LOS DATOS EN EL LAYOUT


    // Create the X-axis band scale
    const x = d3.scaleBand()
      .range([0, this.width])
      .domain(data.map(d => d.nombreProducto))
      .padding(0.2);

    // Draw the X-axis on the DOM
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end").style("text-anchor", "end").style("font-size", this.graphXaxisFontSize)

    // Create the Y-axis band scale
    // console.log(data);
    const y = d3.scaleLinear()

      .domain([0, data[0] ? data[0].cantidad : 1])
      .range([this.height, 0]);

    // Draw the Y-axis on the DOM
    this.svg.append("g")
      .call(d3.axisLeft(y));

    // Create and fill the bars
    this.svg.selectAll("bars")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", d => x(d.nombreProducto))
      .attr("y", d => y(d.cantidad))
      .attr("width", x.bandwidth())
      .attr("height", (d) => this.height - y(d.cantidad))
      .attr("fill", "#498bfc");
  }


}
