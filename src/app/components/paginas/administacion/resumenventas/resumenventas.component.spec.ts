import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenventasComponent } from './resumenventas.component';

describe('ResumenventasComponent', () => {
  let component: ResumenventasComponent;
  let fixture: ComponentFixture<ResumenventasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenventasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenventasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
