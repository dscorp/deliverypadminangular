import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFirestore} from '@angular/fire/firestore';
import {EstablecimientoService} from '../paginas/administacion/establecimiento.service';
import {Apoyo} from '../../domain/entity/apoyo';





@Injectable({
  providedIn: 'root'
})
export class ApoyoService {

  constructor(private db: AngularFireDatabase,
              private firestore: AngularFirestore,
              private establecimientoService: EstablecimientoService,
              ) {

  }


  get() {
    return this.db.list<Apoyo>('Apoyos',ref => ref.orderByChild("estado").equalTo("porAtender"));

  }


  update(idApoyo: string,data:any) {
      return this.db.object("Apoyos/"+idApoyo).update(data)
  }

  save(apoyo:Apoyo)
  {
    return this.db.object("Apoyos/"+apoyo.id).set(apoyo)
  }
}
