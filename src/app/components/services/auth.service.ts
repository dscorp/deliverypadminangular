import { Injectable } from '@angular/core';
import { UserFirebase } from 'src/app/domain/entity/userfirebase';
import firebase, { User } from 'firebase/app'
import { UsersService } from './users.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  provider = new firebase.auth.FacebookAuthProvider()
  userToken: string;

  public singedUser = new UserFirebase();


  constructor(private usersService: UsersService
  ) {

    this.leerToken()
    this.leerUser()

   }


  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userid');
    localStorage.removeItem('username');
    localStorage.removeItem('userphone');
  }

  // loginFacebook( usuario: UserFirebase ) {

  //   const authData = {
  //     ...usuario,
  //     returnSecureToken: true
  //   };

  //   return this.http.post(
  //     `${ this.url }/verifyPassword?key=${ this.apikey }`,
  //     authData
  //   ).pipe(
  //     map( resp => {
  //       this.guardarToken( resp['idToken'] );
  //       return resp;
  //     })
  //   );

  // }

  private guardarToken(idToken: string) {

    this.userToken = idToken;
    localStorage.setItem('token', idToken);

    let hoy = new Date();
    hoy.setSeconds(3600);

    localStorage.setItem('expira', hoy.getTime().toString());


  }

  guardarUser(fireUser:UserFirebase)
  {
    this.singedUser=fireUser


    localStorage.setItem('userid', fireUser.id);
    localStorage.setItem('username', fireUser.nombreCompleto);
    localStorage.setItem('userphone', fireUser.telefono);
  }

  async facebookAuth() {

    var fbuser;

    var a = firebase.auth().signInWithPopup(this.provider).then((result: any) => {
      // This gives you a Facebook Access Token. You can use it to access the Facebook API.
      fbuser = result.user;
      return fbuser;

    });

    var b = a.then(fbuser => {

      var userFirebase = firebase.auth().currentUser;

      return userFirebase.getIdToken().then(token => {
        this.guardarToken(token);

        var userRealTime = new UserFirebase();
        userRealTime.id = userFirebase.uid;
        userRealTime.nombreCompleto = fbuser.displayName;
        userRealTime.tipoUsuario = 'cliente';


        return userRealTime;
      })

    })



    return b;

  }



  leerUser() {

    if ( localStorage.getItem('userid') ) {
      this.singedUser.id= localStorage.getItem('userid');
      this.singedUser.nombreCompleto= localStorage.getItem('username')
      this.singedUser.telefono= localStorage.getItem('userphone')

    } else {
     this.singedUser=null;
    }

    return  this.singedUser;

  }

  leerToken() {

    if ( localStorage.getItem('token') ) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }

    return this.userToken;

  }


  estaAutenticado(): boolean {

    if ( this.userToken.length < 2 ) {
      return false;
    }

    const expira = Number(localStorage.getItem('expira'));
    const expiraDate = new Date();
    expiraDate.setTime(expira);

    if ( expiraDate > new Date() ) {
      //al darle clear  no se guardan datos en local stog
      // localStorage.clear();
      return true;
    } else {
      return false;
    }


  }

}
