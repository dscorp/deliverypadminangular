import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class CategoriaEstablecimientoService {
  

  constructor(private database:AngularFireDatabase) { }

getAll()
{
  return this.database.list("CategoriaEstablecimiento");
}

}
