import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { CategoriaPropiaEstablecimiento } from 'src/app/domain/entity/categoriaPropiaEstablecimiento';

@Injectable({
  providedIn: 'root'
})
export class CategoriasPropiasestabService {

  constructor(private db: AngularFireDatabase) { }



  create(categoria: CategoriaPropiaEstablecimiento, idUsers: string) {
    this.db.list('CategoriasPropiasEstablecimientos/' + idUsers).push(categoria);
  }


  getByEstablecimiento(idEstablecimiento: string) {
  return this.db.list<CategoriaPropiaEstablecimiento>('CategoriasPropiasEstablecimientos/'+idEstablecimiento);

  }


}
