import { Injectable } from '@angular/core';
import {Establecimiento} from '../../../domain/entity/establecimiento';
import {Producto} from '../../../domain/entity/producto';

@Injectable({
  providedIn: 'root'
})
export class ListaproductosService {

   establecimientoSelected:Establecimiento;

   productoSeleccionado:Producto;
  constructor() { }
}
