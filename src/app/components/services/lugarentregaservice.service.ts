import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { LugarEntrega } from 'src/app/domain/entity/lugarEnttrega';

@Injectable({
  providedIn: 'root'
})
export class LugarentregaserviceService {

  constructor(private db: AngularFireDatabase) {

  }


  getlugaresEntrega()
  {
    return this.db.list<LugarEntrega>('LugarEntrega').valueChanges();
  }
}
