import { Injectable } from '@angular/core';
import {Producto} from "../../domain/entity/producto";
import {AngularFireDatabase} from "@angular/fire/database";

@Injectable({
  providedIn: 'root'
})
export class MapServiceService {

  constructor(private db: AngularFireDatabase) { }


  getRepartidoresLibres()
  {
    return this.db.list('repartidores_trabajando');
  }
}
