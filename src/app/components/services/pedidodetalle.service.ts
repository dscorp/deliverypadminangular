import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { PedidoDetalle } from 'src/app/domain/entity/pedidodetalle';

@Injectable({
  providedIn: 'root'
})
export class PedidodetalleService {

  constructor(private db: AngularFireDatabase) { }




  atomicSave(map)
  {
    return this.db.object('').update(map);
  }


  getByPedido(idPedido: string) {

    return this.db.list<any>('PedidoDetalleV2',  ref => ref.orderByChild('idPedido').equalTo(idPedido));
  }

}
