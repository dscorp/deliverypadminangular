import { Injectable } from '@angular/core';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { Pedido } from 'src/app/domain/entity/pedido';
import { PedidoDetalle } from 'src/app/domain/entity/pedidodetalle';
import { PedidoDetalleEr } from 'src/app/domain/entity/pedidodetalleer';
import { PedidoMultiple } from 'src/app/domain/entity/pedidomultimple';
import { EstablecimientoService } from '../paginas/administacion/establecimiento.service';
import {Pedidon} from "../../domain/entity/Pedidon";


@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  public detallesPedido: Array<PedidoDetalleEr> = new Array();

  public pedidosList: Array<PedidoMultiple> = new Array();



  constructor(private db: AngularFireDatabase,
    private firestore: AngularFirestore,
    private establecimientoService: EstablecimientoService) {


  }


  atomicSave(batch: firebase.firestore.WriteBatch) {
    return batch.commit();
  }



  save(pedido: Pedido) {
    return this.db.object(`Pedido/${pedido.id}`).set(pedido);
  }

  calcularMontoTotal(): number {
    var montoTotal: number = 0;
    this.pedidosList.forEach(pdl => {
      pdl.pedioDetalles.forEach(pd => {
        montoTotal = +montoTotal + pd.cantidad * pd.precioVenta;
      })
    })

    return montoTotal
  }


  calculartotalitems(): number {
    var totalitems: number = 0;
    this.pedidosList.forEach(pdl => {
      pdl.pedioDetalles.forEach(pd => {
        totalitems = +totalitems + pd.cantidad;
      })
    })

    return totalitems
  }

  agregarPedidoDetalleEr(pd: PedidoDetalleEr, idEstablecimientoSeleccionado: string) {

    pd.idEstalbecimiento = idEstablecimientoSeleccionado;

    this.establecimientoService.getById(pd.idEstalbecimiento).valueChanges().subscribe(data => {

      const establecimiento = data;

      var element = this.pedidosList.find(e => e.idestablecimiento === pd.idEstalbecimiento)


      if (element == null) {
        //SI NO EXISTE ELEMENTO ENTONCES AGREGA UN ELEMENTO NUEVO
        var pedidoMultiple: PedidoMultiple = new PedidoMultiple();

        //SE ASIGNAN LOS VALORES  ID, NOMBRE ESTABLECIMIENTO E IMAGEN AL OBJ PEDIDOMULTIPLE
        pedidoMultiple.idestablecimiento = pd.idEstalbecimiento;
        pedidoMultiple.nombreEstablecimiento = establecimiento.nombre;
        pedidoMultiple.imagen = establecimiento.logo;

        var pedidoDetalle: PedidoDetalle = new PedidoDetalle();

        //falta el id de pedidoDetalle

        pedidoDetalle.cantidad = pd.cantidad;
        pedidoDetalle.idProducto = pd.producto.id;
        pedidoDetalle.nombreProducto = pd.producto.nombre;
        pedidoDetalle.nota = pd.nota;
        pedidoDetalle.precioVenta = pd.producto.precio;
        pedidoDetalle.imagenProducto = pd.producto.imagen;

        pedidoMultiple.pedioDetalles.push(pedidoDetalle)
        this.pedidosList.push(pedidoMultiple);

      }
      else {
        //SI EXISTE COGE EL ELEMENTO EXISTENTE Y AGREGA EL DETALLEPEDIDO EN SU ARRAY INTERNO
        var pedidoDetalle: PedidoDetalle = new PedidoDetalle();

        //falta el id de pedidoDetalle

        pedidoDetalle.cantidad = pd.cantidad;
        pedidoDetalle.idProducto = pd.producto.id;
        pedidoDetalle.nombreProducto = pd.producto.nombre;
        pedidoDetalle.nota = pd.nota;
        pedidoDetalle.precioVenta = pd.producto.precio;

        pedidoDetalle.imagenProducto = pd.producto.imagen;
        element.pedioDetalles.push(pedidoDetalle)

      }

    });

    // console.log('ests es el pdl final',this.pedidosList);
  }

  eliminarTodosLosPedidos() {
    this.pedidosList = new Array<PedidoMultiple>();
  }

  eliminarPedidoDetalleEr(pd: PedidoDetalle) {

    this.pedidosList.forEach((ped, index1) => {
      ped.pedioDetalles.forEach((item, index2) => {
        if (item.nombreProducto === pd.nombreProducto) {
          ped.pedioDetalles.splice(index2, 1);
          if (ped.pedioDetalles.length == 0) {
            this.pedidosList.splice(index1, 1)
          }
          return;
        }
      });

    });


  }




  getPedidosPorConfirmar() {
    return this.db.list<Pedidon>('Pedido', ref => ref.orderByChild('estado').equalTo('porConfirmar'))

  }



  getPedidosEnEstablecimiento() {
    return this.db.list<Pedidon>('Pedido', ref => ref.orderByChild('estado').equalTo('recibido'))

  }

  getOrdenesTransporteAtendidas() {
    return this.db.list<any>("ordtransporte", ref => ref.orderByChild('estado').equalTo('atendido'))
  }







  actualizarEstadoDePedido(idPedido: string, idEstablecimiento: string, estado: string,) {

    return this.db.object("Pedido/"+idPedido).update
      (
        {
          idEst_Estado: idEstablecimiento+"_"+estado,
          estado: estado
        });
  }

  setFinalizado(idpedido) {
    return this.db.object(`Pedido/${idpedido}`).update({ estado: 'finalizado' });
  }
}
