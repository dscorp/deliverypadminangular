import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Producto } from 'src/app/domain/entity/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  productosList: AngularFireList<Producto>;
  constructor(private db: AngularFireDatabase) { }



  getProductosByEstablecimiento(idEstablecimiento:string)
  {
    return this.productosList =  this.db.list<Producto>('Productos/'+idEstablecimiento);
  }

  getProductosByEstablecimientoV2(idEstablecimiento:string)
  {
    return this.productosList =  this.db.list<Producto>('ProductosV2',ref => ref.orderByChild('idEstablecimiento').equalTo(idEstablecimiento));
  }

  getById(idEstablecimiento:string,idProducto:string)
  {
    return this.db.object<Producto>('Productos/'+idEstablecimiento+'/'+idProducto);
  }

  update(producto:Producto)
  {
    return this.db.object("ProductosV2/"+producto.id).update(producto)
  }
  // getProductosByEstablecimientos(idEstablecimiento:string,nombre:string)
  // {
  //   return this.productosList =  this.db.list<Producto>('Productos/'+idEstablecimiento+nombre);
  // }


}
