import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {Promocion} from '../../domain/entity/Promocion';
import {Producto} from '../../domain/entity/producto';

@Injectable({
  providedIn: 'root'
})
export class PromocionesService {

  constructor( private db:AngularFireDatabase) { }


  save(promocion:Promocion)
  {
    return this.db.object("Promociones/"+promocion.id).set(promocion)

  }

  get(idEstablecimiento:string)
  {
    return this.db.list("Promociones", ref => ref.orderByChild("idEstablecimiento").equalTo(idEstablecimiento))
  }

}
