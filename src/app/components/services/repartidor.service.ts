import { Injectable } from '@angular/core';
import {AngularFireDatabase} from "@angular/fire/database";

@Injectable({
  providedIn: 'root'
})
export class RepartidorService {

  constructor(private db:AngularFireDatabase) { }


  getRepartidoresTrabajando()
  {
    return this.db.list("Repartidor",ref => ref.orderByChild("trabajando").equalTo(true))
  }


  getRepartidorById(idRepartidor:string)
  {
    return this.db.object("Repartidor/"+idRepartidor)
  }

  update(idRepartidor:string,data:any)
  {
    return this.db.object("Repartidor/"+idRepartidor).update(data)
  }

}
