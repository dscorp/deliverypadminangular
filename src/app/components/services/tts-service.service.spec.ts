import { TestBed } from '@angular/core/testing';

import { TTsServiceService } from './tts-service.service';

describe('TTsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TTsServiceService = TestBed.get(TTsServiceService);
    expect(service).toBeTruthy();
  });
});
