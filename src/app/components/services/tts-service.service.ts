import {Injectable} from '@angular/core';
import Speech from 'speak-tts';

@Injectable({
  providedIn: 'root'
})
export class TTsServiceService {
  private speech = new Speech();

  constructor() {
    this.initSpeech()
  }


  private initSpeech() {
    this.speech.setLanguage('es-MX');
  }

  speak(textToRead: string) {
    this.speech.speak({
      text: textToRead
    })
  }
}
