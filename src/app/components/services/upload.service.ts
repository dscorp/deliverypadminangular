import {Injectable} from '@angular/core';
import firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  private storageRef = firebase.storage().ref();
  private CARPETA_IMAGENES = 'imagenes_establecimientos';

  constructor() {
  }

  eliminarImagenByUrl(urlImagen:string)
  {
    this.storageRef.storage.refFromURL(urlImagen).delete();
  }



  getImageUrl(nombreImagen:string)
  {
    return this.storageRef.child(`${this.CARPETA_IMAGENES}/${nombreImagen}`).getDownloadURL()
  }


  uploadProductoImage(archivo: File) {

    return this.storageRef.child(`${this.CARPETA_IMAGENES}/${archivo.name}`)
      .put(archivo);
  }

}
