import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {UserFirebase} from 'src/app/domain/entity/userfirebase';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private db: AngularFireDatabase) { }




  getbyuid(uid:string)
  {
    return this.db.object(`Users/${uid}`)
  }

  save(user: UserFirebase) {
    return this.db.object('Users/' + user.id).set(user);
  }

  getByPhone(busqueda: string) {
    return this.db.list('Users', query => query.orderByChild('telefono').startAt(busqueda).endAt(busqueda + '\uf8ff'));
  }

  cambiarAEstablecimiento(idUsers: string) {
    return this.db.object("Users/" + idUsers).update({ "tipoUsuario": "establecimiento" });
  }

  getallusers() {
    return this.db.list('Users');
  }



  fixFecha() {
    return this.db.list<UserFirebase>('Users').valueChanges().subscribe( users =>{
      for (let user of users) {
        if(user.fechaRegistro==null || user.fechaRegistro==undefined)
        {
        this.db.object('Users/'+user.id).update({fechaRegistro:'25/09/2020'}).then()
        }

      }
    })
  }
}
