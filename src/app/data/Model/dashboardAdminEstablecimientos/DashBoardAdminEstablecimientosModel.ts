import {Injectable} from '@angular/core';
import {OrdenTransporte} from "../../../domain/entity/OrdenTransporte";
import {NetworkCallBack} from "../../../domain/entity/NetworkCallBack";
import {OrdenTransporteRepository} from "../../../domain/repository/OrdenTransporteRepository";
import {PedidoRepository} from "../../../domain/repository/PedidoRepository";
import {Establecimiento} from "../../../domain/entity/establecimiento";
import {RepartidorRepository} from "../../../domain/repository/RepartidorRepository";
import {Repartidor} from "../../../domain/entity/Repartidor";
import {LugarEntrega} from "../../../domain/entity/lugarEnttrega";
import {LugarEntregaRepository} from "../../../domain/repository/LugarEntregaRepository";
import {EstablecimientoRepository} from "../../../domain/repository/EstablecimientoRepository";
import {DashBoardAdminEstablecimientosMVP} from "../../mvpDefinitions/dashboardAdminEstablecimientos/DashBoardAdminEstablecimientosMVP";

@Injectable({
  providedIn: 'root'
})
export class DashBoardAdminEstablecimientosModel implements DashBoardAdminEstablecimientosMVP.Model {

  constructor(
    private ordenTransporteCrud: OrdenTransporteRepository,
    private pedidoCrud: PedidoRepository,
    private establecimientoCrud: EstablecimientoRepository,
    private repartidorCrud: RepartidorRepository,
    private lugarEntregaCrud: LugarEntregaRepository
  ) {
  }

  createOrdenTransporte(ordenTransporte: OrdenTransporte, networkCallBack: NetworkCallBack<OrdenTransporte>) {

    this.ordenTransporteCrud.create(ordenTransporte, new class implements NetworkCallBack<OrdenTransporte> {

      onComplete(ordenTransporte: OrdenTransporte) {
        networkCallBack.onComplete(ordenTransporte)
      }

      onException(e: OrdenTransporte) {
        networkCallBack.onException(e)
      }
    })

  }

  updateOrdenTransporte(idOrdenTranseporte: String, dataforUpdate: any, networkCallback: NetworkCallBack<void>) {

    this.ordenTransporteCrud.update(idOrdenTranseporte, dataforUpdate, new class implements NetworkCallBack<void> {

      onComplete(result: void) {
        networkCallback.onComplete(result)
      }

      onException(e: any) {
        networkCallback.onException(e)
      }
    })

  }


  //PEDIDOS
  updateEstadoDePedido(idPedido: String, dataForUpdate: any, networkCallback: NetworkCallBack<void>) {
    this.pedidoCrud.updateEstadoDePedido(idPedido, dataForUpdate, networkCallback)
  }

  getDetallesByPedido(idPedido: string, networkCallback: NetworkCallBack<any>) {
    this.pedidoCrud.getDetallesByPedido(idPedido, networkCallback)
  }

  //Establecimiento

  getEstablecimientos(networkCallback: NetworkCallBack<Establecimiento[]>) {
    this.establecimientoCrud.getAll(networkCallback)
  }

  //REPARTIDOR

  getRepartidoresTrabajando(networkCallback: NetworkCallBack<Repartidor[]>) {
    this.repartidorCrud.getRepartidoresTrabajando(networkCallback)
  }

  //LUGAR  ENTREGA

  getLugaresEntrega(networkCallback: NetworkCallBack<LugarEntrega[]>) {
    this.lugarEntregaCrud.getAll(networkCallback)
  }


}
