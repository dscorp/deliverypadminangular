import {OrdenesTransporteAtendidasMVP} from "../../mvpDefinitions/dashboardControlPedidos/OrdenesTransporteAtendidasMVP";
import {NetworkCallBack} from "../../../domain/entity/NetworkCallBack";
import {PedidoDetalle} from "../../../domain/entity/pedidodetalle";
import {OrdenTransporteRepository} from "../../../domain/repository/OrdenTransporteRepository";
import {PedidoRepository} from "../../../domain/repository/PedidoRepository";
import {Injectable} from "@angular/core";

@Injectable()
export class OrdenTranporteAtendidaModel implements OrdenesTransporteAtendidasMVP.Model {

  constructor(private ordenTransporteRepository: OrdenTransporteRepository,
              private pedidoRepository: PedidoRepository,
  ) {
  }


  loadPedidoDetalle(idPedido, networkCallBack: NetworkCallBack<PedidoDetalle[]>) {
    this.pedidoRepository.getDetallesByPedido(idPedido, networkCallBack)
  }

  updateOrdeTransporte(id: String, dataForUpdate: any, networkCallBack: NetworkCallBack<void>) {
    this.ordenTransporteRepository.update(id, dataForUpdate, networkCallBack)
  }

}
