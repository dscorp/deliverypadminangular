import {PedidosAceptadosMVP} from "../../mvpDefinitions/dashboardControlPedidos/PedidosAceptadosMVP";
import {PedidoDetalle} from "../../../domain/entity/pedidodetalle";
import {NetworkCallBack} from "../../../domain/entity/NetworkCallBack";
import {PedidoRepository} from "../../../domain/repository/PedidoRepository";
import {Injectable} from "@angular/core";

@Injectable()
export class PedidosAceptadosModel implements PedidosAceptadosMVP.Model {

  constructor(
    private pedidoRepository: PedidoRepository
  ) {
  }

  actualizarEstadoPedido(idpedido, dataForUpdate: any, networkCallBack: NetworkCallBack<void>): void {
    this.pedidoRepository.updateEstadoDePedido(idpedido, dataForUpdate, networkCallBack)
  }

  getDetallesPedido(id: string, networkCallBack: NetworkCallBack<PedidoDetalle[]>): void {
    this.pedidoRepository.getDetallesByPedido(id, networkCallBack)
  }

}
