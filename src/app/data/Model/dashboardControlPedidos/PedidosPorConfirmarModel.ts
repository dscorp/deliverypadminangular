
import {PedidoRepository} from "../../../domain/repository/PedidoRepository";
import {NetworkCallBack} from "../../../domain/entity/NetworkCallBack";
import {PedidoDetalle} from "../../../domain/entity/pedidodetalle";
import {Injectable} from "@angular/core";
import {PedidosPorConfirmarMVP} from "../../mvpDefinitions/dashboardControlPedidos/PedidosPorConfirmarMVP";

@Injectable()
export class PedidosPorConfirmarModel implements PedidosPorConfirmarMVP.Model {
  constructor(
    private pedidoRepository: PedidoRepository
  ) {
  }

  loadPedidoDetalle(id: string, networkCallback: NetworkCallBack<PedidoDetalle[]>) {
    this.pedidoRepository.getDetallesByPedido(id,networkCallback)
  }

  updateEstadoDePedido(idPedido: String, dataForUpdate: any, networkCallback: NetworkCallBack<void>) {
    this.pedidoRepository.updateEstadoDePedido(idPedido,dataForUpdate,networkCallback)
  }


}
