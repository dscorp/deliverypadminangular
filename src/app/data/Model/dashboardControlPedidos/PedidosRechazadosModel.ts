import {PedidoRepository} from "../../../domain/repository/PedidoRepository";
import {Injectable} from "@angular/core";
import {PedidosRechazadosMVP} from "../../mvpDefinitions/dashboardControlPedidos/PedidosRechazadosMVP";
import {Observable} from "rxjs";
import {Pedidon} from "../../../domain/entity/Pedidon";

@Injectable()
export class PedidosRechazadosModel implements PedidosRechazadosMVP.Model {

  constructor(
    private pedidoRepository: PedidoRepository
  ) {
  }

  getRechazadosByDate(fecha: any): Observable<Pedidon[]> {
    return this.pedidoRepository.getRechazadosByDate(fecha)
  }


}
