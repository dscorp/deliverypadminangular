export class FirebaseToArrayAdapter {

  static parseFirebaseDatabaseToArray(data) {
    let arr = []
    Object.keys(data).forEach(key => {
      arr.push(data[key])
    })
    return arr
  }

}
