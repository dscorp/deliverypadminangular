import {OrdenTransporte} from "../../../domain/entity/OrdenTransporte";
import {NetworkCallBack} from "../../../domain/entity/NetworkCallBack";
import {Establecimiento} from "../../../domain/entity/establecimiento";
import {Repartidor} from "../../../domain/entity/Repartidor";
import {LugarEntrega} from "../../../domain/entity/lugarEnttrega";

export module DashBoardAdminEstablecimientosMVP {


  export abstract class Model {

    //ORDEN TRANSPORTE
    abstract createOrdenTransporte(ordenTransporte: OrdenTransporte, networkCallBack: NetworkCallBack<OrdenTransporte>)

    abstract updateOrdenTransporte(idOrdenTranseporte: String, dataforUpdate: any, networkCallback: NetworkCallBack<void>)

//PEDIDO

    abstract updateEstadoDePedido(idPedido: String, dataForUpdate: any, networkCallback: NetworkCallBack<void>)

    abstract getDetallesByPedido(idPedido: string, networkCallback: NetworkCallBack<any>)

    //Establecimiento

    abstract getEstablecimientos(networkCallback: NetworkCallBack<Establecimiento[]>)

//REPARTIDOR
    abstract getRepartidoresTrabajando(networkCallback: NetworkCallBack<Repartidor[]>)


//REPARTIDOR
    abstract getLugaresEntrega(networkCallback: NetworkCallBack<LugarEntrega[]>)
  }




  export abstract class View{
    abstract onLugaresEntregaReady(lugares:LugarEntrega[])
    abstract onEstablecimientosReady(result: Establecimiento[])
  }




  export abstract class Presenter {

    //ORDEN TRANSPORTE
    abstract createOrdenTransporte(ordenTransporte: OrdenTransporte)

    abstract updateOrdenTransporte(idOrdenTranseporte: String, dataforUpdate: any)

//PEDIDO

    abstract updateEstadoDePedido(idPedido: String, dataForUpdate: any)

    abstract getDetallesByPedido(idPedido: string)


    //Establecimiento

    abstract getEstablecimientos()


//REPARTIDOR
    abstract getRepartidoresTrabajando()


//REPARTIDOR
    abstract getLugaresEntrega()

    abstract  setVie(view:DashBoardAdminEstablecimientosMVP.View)

  }

}
