import {OrdenTransporte} from "../../../domain/entity/OrdenTransporte";
import {NetworkCallBack} from "../../../domain/entity/NetworkCallBack";
import {PedidoDetalle} from "../../../domain/entity/pedidodetalle";

export module OrdenesTransporteAtendidasMVP {

  export abstract class Model {

    abstract loadPedidoDetalle(idPedido, networkCallBack: NetworkCallBack<PedidoDetalle[]>)

    abstract updateOrdeTransporte(id: String, dataForUpdate: any, networkCallBack: NetworkCallBack<void>)
  }

  export abstract class Presenter {

    abstract getOrdenesTransporteAtendidas()

    abstract loadPedidoDetalle(idPedido)

    abstract updateOrdeTransporte(id, dataForUpdate: any)

    abstract setView(view: OrdenesTransporteAtendidasMVP.View)
  }

  export abstract class View {

    abstract Speak(message: string)

    abstract onOrdenesTransporteAtendidasChange(data: OrdenTransporte[])

    abstract onDetallesPedidoReady(result: PedidoDetalle[])

    showInforDialog(message: string) {

    }
  }


}
