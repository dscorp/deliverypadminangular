import {Pedidon} from "../../../domain/entity/Pedidon";
import {NetworkCallBack} from "../../../domain/entity/NetworkCallBack";
import {PedidoDetalle} from "../../../domain/entity/pedidodetalle";

export module PedidosAceptadosMVP {

  export abstract class Model {

    abstract getDetallesPedido(id: string, networkCallBack: NetworkCallBack<PedidoDetalle[]>)

    abstract actualizarEstadoPedido(idpedido, dataForUpdate: any, networkCallBack: NetworkCallBack<void>)
  }

  export abstract class Presenter {
    abstract actualizarEstadoPedido(idpedido, dataForUpdate: any)

    abstract loadPedidoDetalle(id: string)

    abstract getPedidosAceptados()

    abstract copyMessageToClipBoard(pedido: Pedidon)

    abstract initPedidoTimeElapsedChecker()

    abstract stopPedidoTimeElapsedChecker()

    abstract setView(view: PedidosAceptadosMVP.View)
  }

  export abstract class View {

    abstract onPedidosAceptadosChanged(data: Pedidon[], newPedido?: Pedidon)

    abstract onDetallesPedidoReady(result: PedidoDetalle[])

    abstract showInfoDialog(message: string)

    abstract notifyDelayedOrder(pedido: Pedidon)

    abstract notifyOrderOnTime(pedido: Pedidon)

    notifyNefastOrder(pedido: Pedidon) {

    }
  }

}
