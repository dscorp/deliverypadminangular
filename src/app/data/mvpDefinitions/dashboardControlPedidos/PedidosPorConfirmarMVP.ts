import {PedidoDetalle} from "../../../domain/entity/pedidodetalle";
import {Pedidon} from "../../../domain/entity/Pedidon";

import {NetworkCallBack} from "../../../domain/entity/NetworkCallBack";

export module PedidosPorConfirmarMVP {

  export abstract class Model {

    abstract updateEstadoDePedido(idPedido: String, dataForUpdate: any, networkCallback: NetworkCallBack<void>)

    abstract loadPedidoDetalle(id: string, networkCallback: NetworkCallBack<PedidoDetalle[]>)
  }

  export abstract class Presenter {

    abstract loadPedidoDetalle(id: string)

    abstract updateEstadoDePedido(idPedido: String, dataForUpdate: any)

    abstract getPedidosPorConfirmar()

    abstract setView(view: PedidosPorConfirmarMVP.View)
  }


  export abstract class View {

    abstract onDetallesPedidoReady(result: PedidoDetalle[])

    abstract showDialogUpdateSucessfull()

    abstract onNewPedidoPorConfirmar(data: Pedidon[], newPedido?: Pedidon)

  }


}
