import {Pedidon} from "../../../domain/entity/Pedidon";
import {Observable} from "rxjs";

export module PedidosRechazadosMVP {

  export abstract class Model {

    abstract getRechazadosByDate(fecha: any): Observable<Pedidon[]>
  }

  export abstract class Presenter {
    abstract getRechazadosByDate(fecha: any)

    abstract setView(view: PedidosRechazadosMVP.View)
  }

  export abstract class View {

    onPedidosRechazadosChanged(data: Pedidon[]) {

    }


    showInfoDialog(message: string) {

    }
  }

}
