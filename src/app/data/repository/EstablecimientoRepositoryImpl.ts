import {NetworkCallBack} from "../../domain/entity/NetworkCallBack";
import {Injectable} from "@angular/core";
import {EstablecimientoRepository} from "../../domain/repository/EstablecimientoRepository";
import {HttpClient} from "@angular/common/http";

import {FirebaseToArrayAdapter} from "../adapters/FirebaseToArrayAdapter";
import {Establecimiento} from "../../domain/entity/establecimiento";
@Injectable()
export class EstablecimientoRepositoryImpl implements EstablecimientoRepository {


  private urlPath = "https://delivery-p.firebaseio.com/Establecimientos.json"

  constructor(private http: HttpClient) {
  }

  getAll(networkCallback: NetworkCallBack<Establecimiento[]>) {

    this.http.get(this.urlPath).subscribe(



      (data:any) =>{
          networkCallback.onComplete(FirebaseToArrayAdapter.parseFirebaseDatabaseToArray(data))
    },




      error => networkCallback.onException(error))
  }


}
