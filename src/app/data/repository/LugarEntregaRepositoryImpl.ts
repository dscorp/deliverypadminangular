import {NetworkCallBack} from "../../domain/entity/NetworkCallBack";
import {HttpClient} from "@angular/common/http";
import {LugarEntregaRepository} from "../../domain/repository/LugarEntregaRepository";
import {LugarEntrega} from "../../domain/entity/lugarEnttrega";
import {FirebaseToArrayAdapter} from "../adapters/FirebaseToArrayAdapter";
import {Injectable} from "@angular/core";
@Injectable()
export class LugarEntregaRepositoryImpl implements LugarEntregaRepository {
  private urlPath = "https://delivery-p.firebaseio.com/LugarEntrega.json"
  constructor(private http: HttpClient) {
  }

  getAll(networkCallback: NetworkCallBack<LugarEntrega[]>) {
    this.http.get(this.urlPath)
      .subscribe((data: any) => {
        networkCallback.onComplete(FirebaseToArrayAdapter.parseFirebaseDatabaseToArray(data))
      }, error => networkCallback.onException(error))
  }

}
