import {OrdenTransporteRepository} from "../../domain/repository/OrdenTransporteRepository";
import {NetworkCallBack} from "../../domain/entity/NetworkCallBack";
import {OrdenTransporte} from "../../domain/entity/OrdenTransporte";
import {AngularFireDatabase} from "@angular/fire/database";
import {Injectable} from "@angular/core";
@Injectable()
export class OrdenTransporteRepositoryImpl implements OrdenTransporteRepository {
  constructor(private db: AngularFireDatabase) {
  }

  update(idOrdenTranseporte: String, data: any, networkCallback: NetworkCallBack<void>) {
     this.db.object("ordtransporte/" + idOrdenTranseporte).update(data)
      .then(networkCallback.onComplete())
      .catch(ex => networkCallback.onException(ex))
  }

  create(ordenTransporte: OrdenTransporte, networkCallback: NetworkCallBack<OrdenTransporte>) {
    ordenTransporte.id = this.db.createPushId()
     this.db.object("ordtransporte/" + ordenTransporte.id)
      .set(ordenTransporte)
      .then(networkCallback.onComplete(ordenTransporte))
      .catch(e => networkCallback.onException(e))
  }

}
