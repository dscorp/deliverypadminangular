import {NetworkCallBack} from "../../domain/entity/NetworkCallBack";
import {AngularFireDatabase} from "@angular/fire/database";
import {PedidoRepository} from "../../domain/repository/PedidoRepository";
import {HttpClient} from "@angular/common/http";
import {FirebaseToArrayAdapter} from "../adapters/FirebaseToArrayAdapter";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Pedido} from "../../domain/entity/pedido";
import {Pedidon} from "../../domain/entity/Pedidon";

@Injectable()
export class PedidoRepositoryImpl implements PedidoRepository {

  private detallesApiURL = "https://delivery-p.firebaseio.com/PedidoDetalleV2.json"
  private pedidosApiURL = "https://delivery-p.firebaseio.com/Pedido.json"

  constructor(
    private db: AngularFireDatabase,
    private http: HttpClient
  ) {
  }

  updateEstadoDePedido(idPedido: String, dataForUpdate: any, networkCallback: NetworkCallBack<void>) {
    this.db.object("Pedido/" + idPedido)
      .update(dataForUpdate)
      .then(networkCallback.onComplete)
      .catch(e => networkCallback.onException(e))
  }

  getDetallesByPedido(idPedido: string, networkCallback: NetworkCallBack<any>) {
    this.http.get(this.detallesApiURL + "?orderBy=\"idPedido\"&equalTo=" + "\"" + idPedido + "\"")
      .subscribe((data: any) => {
        networkCallback.onComplete(FirebaseToArrayAdapter.parseFirebaseDatabaseToArray(data))

      }, error => {
        networkCallback.onException(error)
      })
  }

  getRechazadosByDate(fecha: any): Observable<Pedidon[]> {
    return this.db.list<Pedidon>("Pedido", ref => ref.orderByChild("created_at").startAt(fecha))
      .valueChanges();
  }

}
