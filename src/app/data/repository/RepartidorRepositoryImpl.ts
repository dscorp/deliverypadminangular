import {NetworkCallBack} from "../../domain/entity/NetworkCallBack";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {FirebaseToArrayAdapter} from "../adapters/FirebaseToArrayAdapter";
import {RepartidorRepository} from "../../domain/repository/RepartidorRepository";
import {Repartidor} from "../../domain/entity/Repartidor";

@Injectable()
export class RepartidorRepositoryImpl implements RepartidorRepository {


  private urlPath = "https://delivery-p.firebaseio.com/Repartidor.json"

  constructor(private http: HttpClient) {
  }

  getRepartidoresTrabajando(networkCallback: NetworkCallBack<Repartidor[]>) {
    this.http.get(this.urlPath + "?orderBy=\"trabajando\"&equalTo=true")
      .subscribe((repartidoresTrabajando: any) => {
          networkCallback.onComplete(FirebaseToArrayAdapter.parseFirebaseDatabaseToArray(repartidoresTrabajando))
        },
        error => networkCallback.onException(error))
  }


}
