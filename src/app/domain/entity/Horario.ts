export interface Horario
{
  haDomingo: number;
  haJueves: number;
  haLunes: number;
  haMartes: number;
  haMiercoles: number;
  haSabado: number;
  haViernes: number;
  hcDomingo: number;
  hcJueves: number;
  hcLunes: number;
  hcMartes: number;
  hcMiercoles: number;
  hcSabado: number;
  hcViernes: number;
  horaAperturaDiaNormal: number;
  horaAperturaDomingo: number;
  horaAperturaSabados: number;
  horaCierreDiaNormal: number;
  horaCierreDomingo: number;
  horaCierreSabados: number;
}
