export interface NetworkCallBack<T>
{
  onComplete(result:T)
  onException(e:T)
}
