export class OrdenTransporte {
  costoDelivery: String
  costoTotal: String
  courierNotfound: boolean
  direccionEntrega: String
  estado: String
  id: String
  idEstablecimiento: String
  idPedido: String
  idUserRepartidor: String
  idrep_est: String
  lugarEntrega: String
  nombreCliente: String
  nombreEstablecimiento: String
  nombreRepartidor: String
  pagaCon: String
  pagoSinVuelto: boolean
  subTotal: String
  telefonoCliente: String
  telefonoReferenciaCliente: String
  fechaPedido: String
  detalles: []
}
