export interface Pedidon
{
  costoDelivery: string;
  costoTotal: string;
  created_at: string;
  direccionEntrega: string;
  entregaInmediata: boolean;
  estado: string;
  horaEntrega: string;
  id: string;
  idEst_Estado: string;
  idEstablecimiento: string;
  idUsers: string;
  imagenEstablecimiento: string;
  lugarEntrega: string;
  nombreCliente: string;
  nombreEstablecimiento: string;
  pagaCon: string;
  pagoSinVuelto: boolean;
  subtotal: string;
  telefonoCliente: string;
  telefonoReferencia: string;
  motivoDeRechazo:string;
  detalles:[]
}
