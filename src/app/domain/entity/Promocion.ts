export class Promocion {
  id: string;
  imageurl: string;
  descripcion: string;
  fechaCreacion: number;
  validezEnDias: number;
  idProducto: string;
  imagenProducto: string;
  nombreProducto: string;
  precioProducto: number;
  idEstablecimiento: string;
  nombreEstablecimiento: string;
  logoEstablecimiento: string;
}
