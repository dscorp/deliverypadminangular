export interface Repartidor {
  id: string;
  nombreRepartidor: string;
  pedidosEnCurso: number;
  trabajando: boolean;
}
