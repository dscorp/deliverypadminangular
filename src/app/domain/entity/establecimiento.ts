import {Horario} from "./Horario";

export interface Establecimiento {
  abierto: boolean;
  categoriaEspecifica: string;
  categoriaEstablecimiento: string;
  categoriaEstablecimiento_nombreEstablecimiento: string;
  descripcion: string;
  direccion: string;
  horario: Horario;
  id: string;
  idUsers: string;
  latitude: number;
  logo: string;
  longitude: number;
  nombre: string;
  prioridad: number;
}
