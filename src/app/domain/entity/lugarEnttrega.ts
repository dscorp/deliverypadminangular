export interface LugarEntrega {
    nombre: string;
    precio: number;
    prioridad: number;
}
