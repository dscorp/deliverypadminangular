export class Pedido{
     id:string
     created_at:string;
     idUsers:string;
     idEstablecimiento:string;
     estado:string;
     direccionEntrega:string;
     nombreCliente:string;
     telefonoCliente:string;
     motivoDeRechazo:string;
      pagoSinVuelto:boolean;
     pagaCon:string;
      entregaInmediata:boolean;
     horaEntrega:string;
     lugarEntrega:string;
     subtotal:number;
     costoDelivery:number;
     costoTotal:number;
     nombreEstablecimiento:string;
     idEst_Estado:string;
     isMultiple:boolean;
     plataforma:string = "web";
}
