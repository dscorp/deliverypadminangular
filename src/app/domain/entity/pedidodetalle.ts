
export class PedidoDetalle {
  cantidad: number;
  fecha: number;
  idEstablecimiento: string;
  idPedido: string;
  idProducto: string;
  imagenEstablecimiento: string;
  nombreEstablecimiento: string;
  nombreProducto: string;
  nota: string;
  precioVenta: number;
  imagenProducto:string
}
