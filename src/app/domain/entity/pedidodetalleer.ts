import { Producto } from './producto';

export class PedidoDetalleEr{
   id:string;
   idEstalbecimiento:string;
   producto:Producto; 
   nombreProducto:string;
   precioVenta:number;
   cantidad:number=1;
   nota:string;
}