import { PedidoDetalle } from './pedidodetalle';

export class PedidoMultiple{
    idestablecimiento:string;
    nombreEstablecimiento:string;
    imagen:string;
    pedioDetalles:Array<PedidoDetalle>= new Array();
}