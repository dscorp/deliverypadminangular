export class Producto {
  id: string;
  nombre: string;
  precio: number;
  disponible: boolean;
  categoria: string;
  categoria_Nombre: string;
  descripcion: string;
  imagen: string;
  prioridad: number;
  idEstablecimiento:string;
  logoEstablecimiento:string;
  nombreEstablecimiento:string;
}
