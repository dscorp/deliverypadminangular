export class UserFirebase {
  id: string;
  nombreCompleto: string;
  telefono: string;
  tipoUsuario: string;
  fechaRegistro: string;
}
