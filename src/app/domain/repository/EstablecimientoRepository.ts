import {NetworkCallBack} from "../entity/NetworkCallBack";
import {Injectable} from "@angular/core";
import {Establecimiento} from "../entity/establecimiento";

@Injectable()
export abstract  class EstablecimientoRepository {
 abstract getAll(networkCallback: NetworkCallBack<Establecimiento[]>)
}
