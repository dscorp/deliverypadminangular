import {NetworkCallBack} from "../entity/NetworkCallBack";
import {Injectable} from "@angular/core";
import {LugarEntrega} from "../entity/lugarEnttrega";

@Injectable()
export abstract class LugarEntregaRepository {
  abstract getAll(networkCallback: NetworkCallBack<LugarEntrega[]>)
}
