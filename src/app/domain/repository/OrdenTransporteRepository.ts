import {OrdenTransporte} from "../entity/OrdenTransporte";
import {NetworkCallBack} from "../entity/NetworkCallBack";


export abstract class OrdenTransporteRepository {
  abstract create(ordenTransporte: OrdenTransporte, networkCallback: NetworkCallBack<OrdenTransporte>)

  abstract update(idOrdenTranseporte: String, data: any, networkCallback: NetworkCallBack<void>)

}
