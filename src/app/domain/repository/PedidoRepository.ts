import {NetworkCallBack} from "../entity/NetworkCallBack";
import {Observable} from "rxjs";
import {Pedidon} from "../entity/Pedidon";

export abstract class PedidoRepository {
  abstract updateEstadoDePedido(idPedido: String, dataForUpdate: any, networkCallback: NetworkCallBack<void>)

  abstract getDetallesByPedido(idPedido: string, networkCallback: NetworkCallBack<any>)

  abstract getRechazadosByDate(fecha: any): Observable<Pedidon[]>

}
