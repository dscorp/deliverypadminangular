import {NetworkCallBack} from "../entity/NetworkCallBack";
import {Injectable} from "@angular/core";
import {Repartidor} from "../entity/Repartidor";

@Injectable()
export abstract class RepartidorRepository {
  abstract getRepartidoresTrabajando(networkCallback: NetworkCallBack<Repartidor[]>)
}
