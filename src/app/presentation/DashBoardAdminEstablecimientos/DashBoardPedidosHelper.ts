import {Pedido} from "../../domain/entity/pedido";

export class DashBoardPedidosHelper
{
  static setViewStateRetrasado(pedido: Pedido): boolean {
    const fechaPedido = new Date(pedido.created_at);
    const now = new Date();
    const difference = now.getTime() - fechaPedido.getTime();
    const resultInMinutes = Math.round(difference / 60000);
    if (resultInMinutes <= 20) {
      return false;
    }
    if (resultInMinutes > 20) {
      return true;
    }
  }
}
