import {OrdenTransporte} from "../../domain/entity/OrdenTransporte";
import {LugarEntrega} from "../../domain/entity/lugarEnttrega";
import {NetworkCallBack} from "../../domain/entity/NetworkCallBack";
import {Establecimiento} from "../../domain/entity/establecimiento";
import {DashBoardAdminEstablecimientosMVP} from "../../data/mvpDefinitions/dashboardAdminEstablecimientos/DashBoardAdminEstablecimientosMVP";

export class DashBoardPresenter implements DashBoardAdminEstablecimientosMVP.Presenter {

  private view: DashBoardAdminEstablecimientosMVP.View;

  constructor(private model: DashBoardAdminEstablecimientosMVP.Model) {
  }

  setVie(view: DashBoardAdminEstablecimientosMVP.View) {
    this.view = view
  }

  createOrdenTransporte(ordenTransporte: OrdenTransporte) {
    let parent = this
  }

  getDetallesByPedido(idPedido: string) {
    let parent = this
    this.model.getDetallesByPedido(idPedido, new class implements NetworkCallBack<any> {
      onComplete(result: any) {

      }

      onException(e: any) {
      }
    })
  }

  getEstablecimientos() {
    let parent = this
    this.model.getEstablecimientos(new class implements NetworkCallBack<Establecimiento[]> {
      onComplete(result: Establecimiento[]) {
        parent.view.onEstablecimientosReady(result)
      }

      onException(e: Establecimiento[]) {
      }
    })
  }

  getLugaresEntrega() {
    let parent = this
    this.model.getLugaresEntrega(new class implements NetworkCallBack<LugarEntrega[]> {
      onComplete(result: LugarEntrega[]) {
        parent.view.onLugaresEntregaReady(result)
      }

      onException(e: any) {
      }
    })
  }

  getRepartidoresTrabajando() {
  }

  updateEstadoDePedido(idPedido: String, dataForUpdate: any) {
  }

  updateOrdenTransporte(idOrdenTranseporte: String, dataforUpdate: any) {
  }


}
