import {Component, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {EstablecimientoService} from '../../components/paginas/administacion/establecimiento.service';
import {Observable, of} from 'rxjs';
import {UsersService} from 'src/app/components/services/users.service';
import {catchError, map} from 'rxjs/operators';
import {CategoriaEstablecimientoService} from 'src/app/components/services/categoria-establecimiento.service';
import {Establecimiento} from 'src/app/domain/entity/establecimiento';
import Swal from 'sweetalert2';
import {AngularFireDatabase} from '@angular/fire/database';
import {CategoriasPropiasestabService} from 'src/app/components/services/categorias-propiasestab.service';
import {CategoriaPropiaEstablecimiento} from 'src/app/domain/entity/categoriaPropiaEstablecimiento';
import Speech from 'speak-tts';
import {PedidosService} from '../../components/services/pedidos.service';
import {Pedido} from '../../domain/entity/pedido';
import {MatTableDataSource} from '@angular/material/table';
import {ApoyoService} from '../../components/services/apoyoservice.service';
import {Apoyo} from '../../domain/entity/apoyo';
import {DialogregistroapoyoComponent} from '../DashBoardControlPedidos/DEPRECATEDLstApoyos/dialogregistroapoyo/dialogregistroapoyo.component';
import {MatDialog, MatSort} from '@angular/material';
import {Pedidon} from "../../domain/entity/Pedidon";

function autocompleteStringValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {

    if (control.value.id == null) {
      return {'invalidAutocompleteString': {value: control.value}};
    } else {
      return null;
    }
  };
}

@Component({
  selector: 'app-save',
  templateUrl: './contenedorprincipal.component.html',
  styleUrls: ['./contenedorprincipal.component.css']
})
export class ContenedorprincipalComponent implements OnInit {
  establecimientoForm: FormGroup;
  cattEstablecimiento: string[] = [];
  usersList: Observable<any[]>;
  categoriaInput = '';
  speech = new Speech();

  horario = {
    haLunes: 12,
    hcLunes: 22,
    haMartes: 12,
    hcMartes: 22,
    haMiercoles: 12,
    hcMiercoles: 22,
    haJueves: 12,
    hcJueves: 22,
    haViernes: 12,
    hcViernes: 22,
    haSabado: 12,
    hcSabado: 22,
    haDomingo: 12,
    hcDomingo: 22,
    horaAperturaDiaNormal: 12,
    horaCierreDiaNormal: 22,
    horaAperturaSabados: 12,
    horaCierreSabados: 22,
    horaAperturaDomingo: 12,
    horaCierreDomingo: 22,

  }

  public autoCompleteControl = new FormControl('',
    {validators: [autocompleteStringValidator(), Validators.required]});

  categoriasList: Observable<any[]>;

  dataSource: MatTableDataSource<Pedido>;
  dataSourceAceptados: MatTableDataSource<Pedidon>;

  data: MatTableDataSource<Apoyo>;


  //ACEPTADOS
  firstTimeAceptados = true;
  numeroRegistrosAceptados: number;

  numeroRegistrosApoyos: number;
  temportalDataAceptados: Array<Pedidon>;

  temportalDataApoyos: Array<Apoyo>;
  public validation_msgs = {
    'autoCompleteControl': [
      {type: 'invalidAutocompleteString', message: 'Usuario No Valido , Seleciona una opcion del componente'},
      {type: 'required', message: 'Debe selecionar un Usuario.'}
    ]
  };
  firsttimeApoyo = true;

  @ViewChild(MatSort, null) sort: MatSort;

  constructor(private fBuilder: FormBuilder,
              private establecimientoService: EstablecimientoService,
              private usersService: UsersService,
              private categoraEstablecimientoService: CategoriaEstablecimientoService,
              private db: AngularFireDatabase,
              private categoriasPropiasEstablecimientoService: CategoriasPropiasestabService,
              private pedidosService: PedidosService,
              private apoyoService: ApoyoService,
              public dialog: MatDialog,
  ) {

    this.initformEstablecimiento();
    this.initSpeech();
    this.getPedidosRecibidosEnEstablecimiento();
    this.getApoyos();
  }


  fixFechas() {
    this.usersService.fixFecha()
  }


  fixHorarios() {
    this.establecimientoService.fixHorarios()

  }

  openDialog() {
    this.dialog.open(DialogregistroapoyoComponent);
  }

  getApoyos() {
    this.apoyoService.get().valueChanges().subscribe(data => {

      this.data = new MatTableDataSource(data);


      if (this.firsttimeApoyo == false) {
        if (this.numeroRegistrosApoyos < data.length) {
          let nuevosregistros: Array<Apoyo>;
          nuevosregistros = data;
          const res = nuevosregistros.filter(item1 =>
            !this.temportalDataApoyos.some(item2 => (item2.id === item1.id)));
          const apoyo: Apoyo = res[0];
          this.speech.speak({
            text: 'El establecimiento ' + apoyo.nombreEstablecimiento + ' está solicitando un apoyo para' + apoyo.lugar + this.parseApoyo(apoyo.tiempo),
          });
          this.temportalDataApoyos = data;
        }
      }

      if (this.firsttimeApoyo == true) {
        this.temportalDataApoyos = data;
        this.numeroRegistrosApoyos = data.length;
        this.firsttimeApoyo = false;
      }


    });
  }

  parseApoyo(tiempo): string {
    if (tiempo === 'Ahora mismo') {
      return 'de inmediato';
    } else {
      return 'dentro de ' + tiempo;
    }

  }


  getPedidosRecibidosEnEstablecimiento() {
    this.pedidosService.getPedidosEnEstablecimiento().valueChanges().subscribe(data => {

      const dataWithButton = data.map(data => {
        return {
          ...data,
          buttons: data.id
        };
      });

      this.dataSourceAceptados = new MatTableDataSource(dataWithButton);
      this.dataSourceAceptados.sort = this.sort;
      if (this.firstTimeAceptados == false) {
        if (this.numeroRegistrosAceptados < data.length) {
          let nuevosregistros: Array<Pedidon>;
          nuevosregistros = data;
          const res = nuevosregistros.filter(item1 =>
            !this.temportalDataAceptados.some(item2 => (item2.id === item1.id)));
          const pedido: Pedidon = res[0];
          this.speech.speak({
            text: 'El pedido de ' + pedido.nombreCliente + ' pasó a preparación en el establecimiento ' + pedido.nombreEstablecimiento,
          });
          this.temportalDataAceptados = data;
        }
      }

      if (this.firstTimeAceptados == true) {
        this.temportalDataAceptados = data;
        this.numeroRegistrosAceptados = data.length;
        this.firstTimeAceptados = false;
      }


    });
  }

  initformEstablecimiento() {
    this.establecimientoForm = this.fBuilder.group({
      'id': ['', Validators.required],
      'idUsers': ['', Validators.required],
      'nombre': ['', Validators.required],
      'direccion': ['', Validators.required],
      'descripcion': ['', Validators.required],
      'latitude': [, Validators.required],
      'longitude': [, Validators.required],
      'categoriaEstablecimiento': ['', [Validators.required]],
      'categoriaEstablecimiento_nombreEstablecimiento': ['', [Validators.required]],
      'abierto': [false, Validators.required],
      'logo': ['', Validators.required],
    });
  }

  ngOnInit() {
    this.getCategorias();
  }

  initSpeech() {
    this.speech.setLanguage('es-MX');
  }

  agregarCategoria() {
    if (this.categoriaInput) {
      this.cattEstablecimiento.push(this.categoriaInput);
    }
  }


  submit(formValue) {

    var establecimiento: Establecimiento = formValue;
    //esto porque sino se genera error al buscar en android por eso todo debe estar en minusculas
    establecimiento.nombre = establecimiento.nombre.toLowerCase();
    establecimiento.horario = this.horario;
    establecimiento.categoriaEspecifica = "pollerias"
    establecimiento.prioridad = 800


    const idUsers = establecimiento.idUsers;
    var a = this.establecimientoService.checkIfUsersIdExistOnEstablecimiento(establecimiento.idUsers).subscribe(snapshot => {
      if (snapshot != null) {

        Swal.fire('Error', 'Ya existe establecimiento registrado para este usuario', 'warning');


        a.unsubscribe();
      } else {
        this.establecimientoService.create(formValue);
        this.usersService.cambiarAEstablecimiento(idUsers);
        if (this.cattEstablecimiento.length > 0) {
          this.cattEstablecimiento.forEach(nombrecat => {
            var categoria = new CategoriaPropiaEstablecimiento();
            //esto porque sino se genera error al buscar en android por eso todo debe estar en minusculas
            categoria.nombre = nombrecat.toLowerCase();
            categoria.prioridad = 1;
            this.categoriasPropiasEstablecimientoService.create(categoria, idUsers);
          });
        }
        Swal.fire('Exito', 'Establecimiento registrado.', 'success');
        a.unsubscribe();
      }
    });

  }

  getCategorias() {
    // this.categoraEstablecimientoService.getAll();
    this.categoriasList = this.categoraEstablecimientoService.getAll().valueChanges();
  }

  guardar() {

    var pedido = {
      'nombre': 'nose',
      'preico': 12
    };

    let map = new Map<string, object>();
    map.set('123', pedido);


    this.db.list('CategoriasPropiasEstablecimientos/123').push(map);

  }


  lookup(phone: string) {

    return this.usersList = this.usersService.getByPhone(phone).valueChanges()
      .pipe(
        // map the item property of the github results as our return object
        map(results => {

          return results;
        }),
        // catch errors
        catchError(_ => {
          return of(null);
        })
      );
  }


  buscar(busqueda) {
    if (busqueda) {
      this.usersList = this.lookup('+51' + busqueda);
    }
  }


  eventselect(idUsers) {
    this.establecimientoForm.get('id').setValue(idUsers);
    this.establecimientoForm.get('idUsers').setValue(idUsers);
  }

  getOptionText(option) {
    if (option.nombreCompleto == null) {
      return;
    }

    return option.nombreCompleto + ' - ' + option.telefono;
  }


}
