import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogData} from '../../../../components/catalogo/lista-productos/modalproducto/modalproducto.component';
import {EstablecimientoService} from '../../../../components/paginas/administacion/establecimiento.service';
import {Establecimiento} from '../../../../domain/entity/establecimiento';
import {NgForm} from '@angular/forms';
import {Apoyo} from '../../../../domain/entity/apoyo';
import {LugarentregaserviceService} from '../../../../components/services/lugarentregaservice.service';
import {LugarEntrega} from '../../../../domain/entity/lugarEnttrega';
import {AngularFireDatabase} from '@angular/fire/database';
import {ApoyoService} from '../../../../components/services/apoyoservice.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialogregistroapoyo',
  templateUrl: './dialogregistroapoyo.component.html',
  styleUrls: ['./dialogregistroapoyo.component.css']
})
export class DialogregistroapoyoComponent implements OnInit {
   establecimientos: Array<Establecimiento>;


  tiempo: any;
  establecimiento: any;
  lugar: any;
  lugaresEntrega: Array<LugarEntrega>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
              private dialogRef: MatDialogRef<DialogregistroapoyoComponent>,
              private establecimientoService: EstablecimientoService,
              private lugaresService: LugarentregaserviceService,
              private database:AngularFireDatabase,
              private apoyoService:ApoyoService,

  ) {

    this.listarTodosLosEtablecimientos();
    this.listarTodoslosLugares();
  }

  ngOnInit() {
  }


  registrar(form: NgForm) {
    const apoyo = new Apoyo();
    apoyo.id =this.database.createPushId()
      apoyo.tiempo = form.value.tiempo;
    const establecimientoSeleccionado = form.value.establecimiento;
    apoyo.idEstablecimiento = establecimientoSeleccionado.id;
    apoyo.nombreEstablecimiento = establecimientoSeleccionado.nombre;
    apoyo.fecha = new Date().getTime();
    apoyo.estado = 'porAtender';

    const lugarSelecionado: LugarEntrega = form.value.lugar;
    apoyo.lugar = lugarSelecionado.nombre;
    apoyo.precio = lugarSelecionado.precio.toString();

    this.apoyoService.save(apoyo).then(data=>{
      Swal.fire("Apoyo registrado exitosamente","","success")
      this.dialogRef.close()
    }).catch(err=>{
      Swal.fire("Ocurrio un error al registrar el apoyo",err.message,"error")
      this.dialogRef.close()
    })
  }

  listarTodoslosLugares() {
    this.lugaresService.getlugaresEntrega().subscribe((lugares: Array<LugarEntrega>) => {
      this.lugaresEntrega = lugares;
    });
  }

  listarTodosLosEtablecimientos() {
    return this.establecimientoService.getEstablecimientos().valueChanges().subscribe((establecimientos: Array<Establecimiento>) => {
      this.establecimientos = establecimientos;
    });
  }


}
