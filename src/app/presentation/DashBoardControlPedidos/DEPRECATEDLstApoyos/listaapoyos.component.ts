
import {MatTableDataSource} from '@angular/material/table';
import {Component, Input, OnInit} from '@angular/core';
import {Apoyo} from '../../../domain/entity/apoyo';
import {Pedido} from '../../../domain/entity/pedido';
import {ApoyoService} from '../../../components/services/apoyoservice.service';
import Swal from 'sweetalert2';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-listaapoyos',
  templateUrl: './listaapoyos.component.html',
  styleUrls: ['./listaapoyos.component.css']
})
export class ListaapoyosComponent implements OnInit {
  @Input('dataSource') dataSource: MatTableDataSource<Apoyo>;
  displayedColumns: string[] = ['nombreEstablecimiento','lugar','tiempo', 'fecha','Accion'];


  expandedElement: any;
  constructor(private apoyoService:ApoyoService
 ) { }

  ngOnInit() {

  }

 rechazarApoyo(idApoyo:string)
  {

    this.apoyoService.update(idApoyo,{estado:"rechazado"})
      .then(it =>{
      });
  }

  aceptarApoyo(idApoyo:string)
  {
      this.apoyoService.update(idApoyo,{estado:"aceptado"})
        .then(it =>{

        });
  }

  definirColorTiempo(tiempo)
  {
    switch (tiempo){
      case 'Ahora mismo':
        return '#E0B88B'
      break;

      case '5 Minutos':
        return 'unset'
        break;

      case '10 Minutos':
        return 'unset'
        break;
    }

  }


}
