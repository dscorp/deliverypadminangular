import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {RepartidorService} from "../../../../components/services/repartidor.service";

@Component({
  selector: 'app-dialog-infro-repartidor',
  templateUrl: './dialog-infro-repartidor.component.html',
  styleUrls: ['./dialog-infro-repartidor.component.css']
})
export class DialogInfroRepartidorComponent implements OnInit {

  nombreRepartidor = ""
  numeroPedidoesEnCurso = ""


  constructor(public dialogRef: MatDialogRef<DialogInfroRepartidorComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogInfoRepartidorData,
              private repartidorService: RepartidorService) {
    this.getinfoRepartidor(data.idRepartidor)
  }

  ngOnInit() {

  }


  getinfoRepartidor(idRepartidor: string) {
    this.repartidorService.getRepartidorById(idRepartidor)
      .valueChanges().subscribe((data: any) => {
      this.nombreRepartidor = data.nombreRepartidor
      this.numeroPedidoesEnCurso = data.pedidosEnCurso
      console.log('esta es la data del repartidor',data)
    })
  }


}

export interface DialogInfoRepartidorData {
  idRepartidor: string;
}
