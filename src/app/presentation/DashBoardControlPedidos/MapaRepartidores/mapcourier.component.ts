import {Component, OnInit} from '@angular/core';
import {MapServiceService} from "../../../components/services/map-service.service";
import {MatDialog} from "@angular/material";
import {DialogInfroRepartidorComponent} from "./dialog-infro-repartidor/dialog-infro-repartidor.component";

@Component({
  selector: 'app-mapcourier',
  templateUrl: './mapcourier.component.html',
  styleUrls: ['./mapcourier.component.css']
})
export class MapcourierComponent implements OnInit {
  lat = -11.235055;
  lng = -77.381540;
  locations: any[] = []
  maptype = 'hybrid'

  constructor(private mapService: MapServiceService, public dialog: MatDialog,) {
  }

  ngOnInit() {
    this.getRepartidoresLibres()
  }

  getRepartidoresLibres() {

    this.mapService.getRepartidoresLibres().snapshotChanges().subscribe((data: any) => {
      this.locations = []
      data.forEach(action => {
        let location = {
          key: action.key,
          ...action.payload.val()
        }
        this.locations.push(location)

      });
    })
  }

  openDialog(location: any): void {
    const dialogRef = this.dialog.open(DialogInfroRepartidorComponent, {
      width: '500px',
      data: {idRepartidor: location.key}
    });
  }

  mostrarDatosRepartidor(location: any) {
    this.openDialog(location)
  }
}
