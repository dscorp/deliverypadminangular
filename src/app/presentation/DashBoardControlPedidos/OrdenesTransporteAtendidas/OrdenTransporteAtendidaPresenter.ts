import {OrdenesTransporteAtendidasMVP} from "../../../data/mvpDefinitions/dashboardControlPedidos/OrdenesTransporteAtendidasMVP";
import {PedidosService} from "../../../components/services/pedidos.service";
import {OrdenTransporte} from "../../../domain/entity/OrdenTransporte";
import {PedidoDetalle} from "../../../domain/entity/pedidodetalle";
import {NetworkCallBack} from "../../../domain/entity/NetworkCallBack";
import {Injectable} from "@angular/core";

@Injectable()
export class OrdenTransporteAtendidaPresenter implements OrdenesTransporteAtendidasMVP.Presenter {

  private view: OrdenesTransporteAtendidasMVP.View

  firstTime: boolean = true;
  detalles: PedidoDetalle[];
  numeroRegistros: number;
  temportalData: Array<OrdenTransporte>;


  constructor(private model: OrdenesTransporteAtendidasMVP.Model,
              private pedidoService: PedidosService,
  ) {
  }

  getOrdenesTransporteAtendidas(): void {
    this.pedidoService.getOrdenesTransporteAtendidas().valueChanges()
      .subscribe(data => {

          if (this.firstTime == false) {

            if (this.numeroRegistros < data.length) {
              let nuevosregistros: Array<OrdenTransporte>;
              nuevosregistros = data;

              let res = nuevosregistros.filter(item1 =>
                !this.temportalData.some(item2 => (item2.id === item1.id)))

              let orden: OrdenTransporte = res[0];

              if (orden.nombreCliente == 'deliverypeapoyo') {
                this.view.Speak(`El apoyo del establecimiento    ${orden.nombreEstablecimiento}  fue asignado al repartidor ${(orden.nombreRepartidor ? orden.nombreRepartidor : "")} `)
              } else {
                this.view.Speak(`El pedido de ${orden.nombreCliente} en el establecimiento ${orden.nombreEstablecimiento} fué asignado al repartidor ${(orden.nombreRepartidor ? orden.nombreRepartidor : '')}`)
              }
              this.temportalData = data;

            }
          }

          if (this.firstTime == true) {
            this.temportalData = data;
            this.numeroRegistros = data.length;
            this.firstTime = false
          }

            this.view.onOrdenesTransporteAtendidasChange(data)
        }
      )
  }

  loadPedidoDetalle(idPedido): void {
    let parent = this
    this.model.loadPedidoDetalle(idPedido,new class implements NetworkCallBack<PedidoDetalle[]> {
      onComplete(result: PedidoDetalle[]) {
        parent.view.onDetallesPedidoReady(result)
      }

      onException(e: PedidoDetalle[]) {
      }

    })
  }

  updateOrdeTransporte(id, dataForUpdate: any): void {
    let parent = this
      this.model.updateOrdeTransporte(id,dataForUpdate, new class implements NetworkCallBack<void> {
        onComplete(result: void) {
          parent.view.showInforDialog("La orden de transporte fue actualizada exitosamente")
        }
        onException(e: void) {
        }
      })
  }

  setView(view: OrdenesTransporteAtendidasMVP.View) {
    this.view = view
  }
}
