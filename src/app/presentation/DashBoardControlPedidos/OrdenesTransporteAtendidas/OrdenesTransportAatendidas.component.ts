import {animate, state, style, transition, trigger} from '@angular/animations';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {OrdenTransporte} from 'src/app/domain/entity/OrdenTransporte';
import {PedidoDetalle} from 'src/app/domain/entity/pedidodetalle';
import {PedidosService} from '../../../components/services/pedidos.service';
import Speech from 'speak-tts'
import {DiaogAsignarordentransporteComponent} from "./diaog-asignarordentransporte/diaog-asignarordentransporte.component";

import {OrdenesTransporteAtendidasMVP} from "../../../data/mvpDefinitions/dashboardControlPedidos/OrdenesTransporteAtendidasMVP";
import {TTsServiceService} from "../../../components/services/tts-service.service";
import {DashBoardAdminEstablecimientosModel} from "../../../data/Model/dashboardAdminEstablecimientos/DashBoardAdminEstablecimientosModel";

@Component({
  selector: 'app-listarordenestransporteatendidas',
  templateUrl: './OrdenesTransportAatendidas.component.html',
  styleUrls: ['./OrdenesTransportAatendidas.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ],
})
export class OrdenesTransportAatendidasComponent implements OnInit, OrdenesTransporteAtendidasMVP.View {
  dataSource: MatTableDataSource<OrdenTransporte>;
  displayedColumns: string[] = ['nombreRepartidor', 'nombreCliente', 'nombreEstablecimiento',
    'lugarEntrega', 'costoTotal', 'telefonoCliente', 'fechaPedido', 'buttons'];
  expandedElement: any;
  firstTime: boolean = true;
  detalles: PedidoDetalle[];
  numeroRegistros: number;
  speech = new Speech()
  temportalData: Array<OrdenTransporte>;

  @ViewChild(MatSort, null) sort: MatSort;

  constructor(
    private ordenTransporteService: PedidosService,
    public dialog: MatDialog,
    private ordentransporteRepository: DashBoardAdminEstablecimientosModel,
    private ttsService: TTsServiceService,
    private presenter: OrdenesTransporteAtendidasMVP.Presenter,
  ) {
    presenter.setView(this)
  }

  ngOnInit() {
    this.presenter.getOrdenesTransporteAtendidas()
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(ordenTransporte: any): void {
    const dialogRef = this.dialog.open(DiaogAsignarordentransporteComponent, {
      width: '550px',
      data: {ordenTransporte: ordenTransporte}
    });
  }

  loadPedidoDetalle(pedido) {
    this.detalles = []
    this.detalles = pedido.detalles
  }

  finalizar(ordenTransporte) {
    const dataForUpdate = {
      estado: "finalizado",
      idrep_est: ordenTransporte.idUserRepartidor + "_finalizado"
    }
    this.presenter.updateOrdeTransporte(ordenTransporte.id, dataForUpdate)
  }

  onOrdenesTransporteAtendidasChange(data: OrdenTransporte[]) {
    this.dataSource = new MatTableDataSource<OrdenTransporte>(data)
    this.dataSource.sort = this.sort;
  }

  Speak(message: string) {
    this.ttsService.speak(message)
  }

  onDetallesPedidoReady(result: PedidoDetalle[]) {
    this.detalles = result
  }

  copyMessageToClipBoard(orden: OrdenTransporte) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';

    var datatotext = `${orden.nombreEstablecimiento.toUpperCase()}\n${orden.nombreCliente} - ${orden.lugarEntrega} ${orden.direccionEntrega}\n\n`;
    orden.detalles.forEach((detalle: PedidoDetalle) => {
      const row = `X${detalle.cantidad} ${detalle.nombreProducto} - S/${detalle.cantidad * detalle.precioVenta}\n`;
      datatotext += row
    })
    datatotext+=`\nSUBTOTAL: S/${orden.subTotal}`
    selBox.value = datatotext
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  showInforDialog(message: string): void {
    console.info(message)
  }
}
