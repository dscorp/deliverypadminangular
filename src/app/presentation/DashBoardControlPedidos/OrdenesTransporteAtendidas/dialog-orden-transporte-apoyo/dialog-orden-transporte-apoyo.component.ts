import {Component, Inject, OnInit} from '@angular/core';
import {DialogData} from "../../../../components/catalogo/lista-productos/modalproducto/modalproducto.component";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {EstablecimientoService} from "../../../../components/paginas/administacion/establecimiento.service";
import {LugarEntrega} from "../../../../domain/entity/lugarEnttrega";
import {LugarentregaserviceService} from "../../../../components/services/lugarentregaservice.service";
import {Establecimiento} from "../../../../domain/entity/establecimiento";
import {NgForm} from "@angular/forms";
import Swal from "sweetalert2";
import {OrdenTransporte} from "../../../../domain/entity/OrdenTransporte";
import {RepartidorService} from "../../../../components/services/repartidor.service";

import {NetworkCallBack} from "../../../../domain/entity/NetworkCallBack";
import {Repartidor} from "../../../../domain/entity/Repartidor";
import {DashBoardAdminEstablecimientosModel} from "../../../../data/Model/dashboardAdminEstablecimientos/DashBoardAdminEstablecimientosModel";

@Component({
  selector: 'app-dialog-orden-transporte-apoyo',
  templateUrl: './dialog-orden-transporte-apoyo.component.html',
  styleUrls: ['./dialog-orden-transporte-apoyo.component.css']
})


export class DialogOrdenTransporteApoyoComponent implements OnInit {
  lugaresEntrega: Array<LugarEntrega>;
  establecimientos: Array<Establecimiento>;

  establecimiento: any;
  lugar: any;
  RepartidorSeleccionado: any = null
  repartidoresTrabajando: any[] = []

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
              private dialogRef: MatDialogRef<DialogOrdenTransporteApoyoComponent>,
              private establecimientoService: EstablecimientoService,
              private lugaresService: LugarentregaserviceService,
              private dashBoardPedidosRepository: DashBoardAdminEstablecimientosModel,
              private repartidorService: RepartidorService,
  ) {

    this.listarTodosLosEtablecimientos();
    this.listarTodoslosLugares();
    this.getRepartidoresTrabajando();
  }

  listarTodoslosLugares() {
    let parent = this
    this.dashBoardPedidosRepository.getLugaresEntrega(new class implements NetworkCallBack<LugarEntrega[]> {
      onComplete(lugares: LugarEntrega[]) {
        parent.lugaresEntrega = lugares;
      }

      onException(e: any) {
      }
    })

  }

  getRepartidoresTrabajando() {
    let parent = this
    this.dashBoardPedidosRepository.getRepartidoresTrabajando(new class implements NetworkCallBack<Repartidor[]> {
      onComplete(repartidoresTrabajando: Repartidor[]) {
        parent.repartidoresTrabajando = repartidoresTrabajando
      }

      onException(e: any) {
      }
    })
  }

  listarTodosLosEtablecimientos() {
    let parent = this
    this.dashBoardPedidosRepository.getEstablecimientos(new class implements NetworkCallBack<Establecimiento[]> {
      onComplete(establecimientos: Establecimiento[]) {
        parent.establecimientos = establecimientos;
      }

      onException(e: any) {
        console.error(e.message)
      }
    })

  }

  registrar(form: NgForm) {
    let parent = this
    const lugarSelecionado: LugarEntrega = form.value.lugar;
    const establecimientoSeleccionado = form.value.establecimiento;
    const ordentransorte = new OrdenTransporte()
    ordentransorte.lugarEntrega = lugarSelecionado.nombre;
    ordentransorte.costoDelivery = lugarSelecionado.precio+"";
    ordentransorte.pagaCon=0+""
    ordentransorte.costoTotal=lugarSelecionado.precio+""
    ordentransorte.idEstablecimiento = establecimientoSeleccionado.id;
    ordentransorte.nombreEstablecimiento = establecimientoSeleccionado.nombre;
    ordentransorte.idUserRepartidor = this.RepartidorSeleccionado.id
    ordentransorte.nombreRepartidor = this.RepartidorSeleccionado.nombreRepartidor
    ordentransorte.idrep_est = this.RepartidorSeleccionado.id + "_atendido"
    ordentransorte.estado = "atendido"
    ordentransorte.nombreCliente = "deliverype apoyo"

    this.dashBoardPedidosRepository.createOrdenTransporte(ordentransorte, new class implements NetworkCallBack<OrdenTransporte> {
      onComplete(result: OrdenTransporte) {
        Swal.fire("Apoyo registrado exitosamente", "", "success")
        parent.dialogRef.close()
      }

      onException(err: any) {
        Swal.fire("Ocurrio un error al registrar el apoyo", err.message, "error")
        parent.dialogRef.close()
      }
    })
  }

  ngOnInit() {
  }

}
