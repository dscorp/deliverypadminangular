import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {RepartidorService} from "../../../../components/services/repartidor.service";
import {NgForm} from "@angular/forms";
import Swal from "sweetalert2";
import {NetworkCallBack} from "../../../../domain/entity/NetworkCallBack";
import {DashBoardAdminEstablecimientosModel} from "../../../../data/Model/dashboardAdminEstablecimientos/DashBoardAdminEstablecimientosModel";

@Component({
  selector: 'app-diaog-asignarordentransporte',
  templateUrl: './diaog-asignarordentransporte.component.html',
  styleUrls: ['./diaog-asignarordentransporte.component.css']
})
export class DiaogAsignarordentransporteComponent implements OnInit {

  repartidoresTrabajando: any[] = []
  RepartidorSeleccionado: any = null
  ordenTransporteSeleccionada: any
  numeroReferencia: string

  constructor(
    public dialogRef: MatDialogRef<DiaogAsignarordentransporteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private repartidorService: RepartidorService,
    private oredenTransporteRepository: DashBoardAdminEstablecimientosModel,
  ) {
    this.ordenTransporteSeleccionada = data.ordenTransporte
    this.numeroReferencia = this.ordenTransporteSeleccionada.telefonoReferenciaCliente
  }

  getRepartidoresTrabajando() {
    this.repartidorService.getRepartidoresTrabajando()
      .valueChanges().subscribe(repartidoresTrabajando => {
      this.repartidoresTrabajando = repartidoresTrabajando
    })
  }

  asignarOrdenARepartidor(f: NgForm) {
    if (f.valid) {
      this.actualizarOrdenTransporte(f.value.rep)
    } else {
      Swal.fire("Debe rellenar los campos correctamente", "", "warning")
    }
  }

  actualizarOrdenTransporte(repartidor: any) {
    let parent = this
    const data = {
      "idUserRepartidor": repartidor.id,
      "nombreRepartidor": repartidor.nombreRepartidor,
      "idrep_est": repartidor.id + "_atendido",
      "telefonoReferenciaCliente": this.numeroReferencia,
    }

    this.oredenTransporteRepository.updateOrdenTransporte(this.ordenTransporteSeleccionada.id, data, new class implements NetworkCallBack<void> {
      onComplete(result: void) {
        Swal.fire("La orden de transporte fue asiganada correctamente al repartidor " + repartidor.nombreRepartidor, "", "success")
        parent.dialogRef.close();
      }

      onException(e: any) {
        Swal.fire("Ocurrio un error " + e.message, "", "error")
        parent.dialogRef.close();
      }
    })
  }

  ngOnInit() {
    this.getRepartidoresTrabajando()
  }
}
