import {PedidosAceptadosMVP} from "../../../data/mvpDefinitions/dashboardControlPedidos/PedidosAceptadosMVP";
import {PedidosService} from "../../../components/services/pedidos.service";
import {PedidoDetalle} from "../../../domain/entity/pedidodetalle";
import {NetworkCallBack} from "../../../domain/entity/NetworkCallBack";
import {Pedidon} from "../../../domain/entity/Pedidon";
import {Injectable} from "@angular/core";

@Injectable()
export class PedidosAceptadosPresenter implements PedidosAceptadosMVP.Presenter {


  private view: PedidosAceptadosMVP.View


  //ACEPTADOS
  private firstTimeAceptados = true;
  private numeroRegistrosAceptados: number;
  private temportalDataEstablecimiento = []
  private timer: NodeJS.Timer;
  private pedidosEnEstablecimiento: Pedidon[] = [];

  constructor(
    private pedidoService: PedidosService,
    private model: PedidosAceptadosMVP.Model
  ) {
  }

  actualizarEstadoPedido(idpedido, dataForUpdate: any) {
    let parent = this
    this.model.actualizarEstadoPedido(idpedido, dataForUpdate, new class implements NetworkCallBack<void> {
      onComplete(result: void) {
        parent.view.showInfoDialog("Se cambio el estado del pedido correctamente")
      }

      onException(e: any) {
      }
    })
  }

  getPedidosAceptados() {
    this.pedidoService.getPedidosEnEstablecimiento().valueChanges().subscribe((data) => {

      this.pedidosEnEstablecimiento = data

      if (this.firstTimeAceptados == false) {
        if (this.numeroRegistrosAceptados < data.length) {

          let nuevosregistrosPorCOnfirmar: Array<Pedidon>;
          nuevosregistrosPorCOnfirmar = data;
          let res = nuevosregistrosPorCOnfirmar.filter(item1 =>
            !this.temportalDataEstablecimiento.some(item2 => (item2.id === item1.id)));
          let newPedido: Pedidon = res[0];

          this.view.onPedidosAceptadosChanged(data, newPedido)
          this.temportalDataEstablecimiento = data;
        } else {
          this.view.onPedidosAceptadosChanged(data)
        }
      }

      if (this.firstTimeAceptados == true) {
        this.view.onPedidosAceptadosChanged(data)
        this.temportalDataEstablecimiento = data;
        this.numeroRegistrosAceptados = data.length;
        this.firstTimeAceptados = false;
      }
    })
  }

  loadPedidoDetalle(id: string) {
    let parent = this
    this.model.getDetallesPedido(id, new class implements NetworkCallBack<PedidoDetalle[]> {
      onComplete(result: PedidoDetalle[]) {
        parent.view.onDetallesPedidoReady(result)
      }

      onException(e: PedidoDetalle[]) {
      }
    })
  }

  setView(view: PedidosAceptadosMVP.View) {
    this.view = view
  }

  copyMessageToClipBoard(pedido: Pedidon) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    var datatotext = `${pedido.nombreEstablecimiento.toUpperCase()}\n${pedido.nombreCliente} - ${pedido.lugarEntrega} ${pedido.direccionEntrega}\n\n`;
    pedido.detalles.forEach((detalle: PedidoDetalle) => {
      const row = `X${detalle.cantidad} ${detalle.nombreProducto} - S/${detalle.cantidad * detalle.precioVenta}\n`;
      datatotext += row
    })
    datatotext += `\nSUBTOTAL: S/${pedido.subtotal}`
    selBox.value = datatotext
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  initPedidoTimeElapsedChecker() {


    this.timer = setInterval(() => {
      console.log('iniciado checker')
      console.log('numero de pedidos:', this.pedidosEnEstablecimiento.length)
      this.pedidosEnEstablecimiento.forEach(pedido => {
        const actualDate = new Date().getTime();
        const orderDate = new Date(pedido.created_at).getTime()
        const dateDifference = ((actualDate - orderDate) / 1000) / 60;

        // if (dateDifference >= 15 && dateDifference < 30)
        //   this.view.notifyOrderOnTime(pedido)
        if (dateDifference >= 25 && dateDifference < 45)
          this.view.notifyDelayedOrder(pedido)
        if (dateDifference >= 45)
          this.view.notifyNefastOrder(pedido)
      })

    }, 180 * 1000);

  }

  stopPedidoTimeElapsedChecker() {
    if (this.timer != undefined) {
      this.timer.unref()
    }
  }


}
