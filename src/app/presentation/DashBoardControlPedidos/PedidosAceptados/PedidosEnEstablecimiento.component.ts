import {animate, state, style, transition, trigger} from '@angular/animations';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {Pedido} from 'src/app/domain/entity/pedido';
import {PedidoDetalle} from 'src/app/domain/entity/pedidodetalle';
import {PedidosService} from '../../../components/services/pedidos.service';
import {PedidodetalleService} from '../../../components/services/pedidodetalle.service';
import {DashBoardPedidosHelper} from "../../DashBoardAdminEstablecimientos/DashBoardPedidosHelper";
import {PedidosAceptadosMVP} from "../../../data/mvpDefinitions/dashboardControlPedidos/PedidosAceptadosMVP";
import {Pedidon} from "../../../domain/entity/Pedidon";
import {TTsServiceService} from "../../../components/services/tts-service.service";

@Component({
  selector: 'app-listarpedidosenestablecimiento',
  templateUrl: './PedidosEnEstablecimiento.component.html',
  styleUrls: ['./PedidosEnEstablecimiento.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ],
})
export class PedidosEnEstablecimientoComponent implements OnInit, OnDestroy, PedidosAceptadosMVP.View {


  dataSource: MatTableDataSource<Pedidon>;
  displayedColumns: string[] = ['nombreEstablecimiento', 'lugarEntrega', 'nombreCliente', 'telefonoCliente', 'created_at', 'buttons'];
  detalles: PedidoDetalle[];
  expandedElement: any;

  @ViewChild(MatSort, null) sort: MatSort;
  private pedidosEnEstablecimiento: Pedidon[] = [];

  constructor(
    private pedidosService: PedidosService,
    private pedidoDetalleService: PedidodetalleService,
    private presenter: PedidosAceptadosMVP.Presenter,
    private ttsService: TTsServiceService,
  ) {
    presenter.setView(this)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  loadPedidoDetalle(pedido: any) {
    this.detalles = []
    this.detalles = pedido.detalles
  }

  actualizarEstadoPedido(idpedido, idestablecimiento, estado) {
    let dataForUpdate = {
      idEst_Estado: idestablecimiento + "_" + estado,
      estado: estado
    }
    this.presenter.actualizarEstadoPedido(idpedido, dataForUpdate)
  }


  onDetallesPedidoReady(result: PedidoDetalle[], newPedido?: Pedidon): void {
    this.detalles = result
  }

  onPedidosAceptadosChanged(data: Pedidon[], newPedido?: Pedidon): void {
    this.pedidosEnEstablecimiento = data
    this.dataSource = new MatTableDataSource<Pedidon>(data)
    this.dataSource.sort = this.sort;
    if (newPedido != undefined) {
      this.ttsService
        .speak(`"El pedido de ${newPedido.nombreCliente}  pasó a preparación en el establecimiento ${newPedido.nombreEstablecimiento}`)
    }
  }

  copyMessageToClipBoard(pedido: Pedidon) {
    this.presenter.copyMessageToClipBoard(pedido)
  }

  showInfoDialog(message: string): void {
    console.info(message)
  }

  ngOnInit() {
    this.presenter.getPedidosAceptados()
    this.presenter.initPedidoTimeElapsedChecker()
  }

  pedidoRestrasado(pedido: Pedido): Boolean {
    return DashBoardPedidosHelper.setViewStateRetrasado(pedido)
  }



  notifyOrderOnTime(pedido: Pedidon) {
  this.ttsService.speak(`El pedido de ${pedido.nombreCliente} en el establecimiento ${pedido.nombreEstablecimiento} ya tiene más de 15 minutos en preparación, se recomienda comunicarse con el establecimiento`)

  }

  notifyDelayedOrder(pedido: Pedidon) {
    this.ttsService.speak(`Atención! pedido retrasado. El pedido de ${pedido.nombreCliente} en el establecimiento ${pedido.nombreEstablecimiento} ya tiene más de 25 minutos en preparación, recomiendo comunicarse con el establecimiento`)

  }
  notifyNefastOrder(pedido: Pedidon): void {
    this.ttsService.speak(`Atención! pedido  con riesgo de ser cancelado. El pedido de ${pedido.nombreCliente} en el establecimiento ${pedido.nombreEstablecimiento} ya tiene más de 45 minutos en preparación, recomiendo comunicarse urgentemente con el establecimiento`)

  }


  ngOnDestroy(): void {
    this.presenter.stopPedidoTimeElapsedChecker()
  }

}
