
import {NetworkCallBack} from "../../../domain/entity/NetworkCallBack";
import {PedidoDetalle} from "../../../domain/entity/pedidodetalle";
import {PedidosService} from "../../../components/services/pedidos.service";
import {Pedidon} from "../../../domain/entity/Pedidon";
import {Injectable} from "@angular/core";
import {PedidosPorConfirmarMVP} from "../../../data/mvpDefinitions/dashboardControlPedidos/PedidosPorConfirmarMVP";

@Injectable()
export class PedidosPorConfirmarPresenter implements PedidosPorConfirmarMVP.Presenter {

  private view: PedidosPorConfirmarMVP.View


  //en establecimiento
  firstTimeEnestablecimiento = true;
  numeroRegistrosEnEstablecimiento: number;
  temportalDataEstablecimiento: Array<Pedidon>;

  constructor(
    private model: PedidosPorConfirmarMVP.Model,
    private pedidoService: PedidosService
  ) {
  }

  loadPedidoDetalle(id: string) {
    let parent = this
    this.model.loadPedidoDetalle(id, new class implements NetworkCallBack<PedidoDetalle[]> {
      onComplete(result: PedidoDetalle[]) {
        parent.view.onDetallesPedidoReady(result)
      }

      onException(e: PedidoDetalle[]) {
      }
    })
  }

  updateEstadoDePedido(idPedido: String, dataForUpdate: any) {

    let parent = this
    this.model.updateEstadoDePedido(idPedido, dataForUpdate, new class implements NetworkCallBack<void> {
      onComplete(result: void) {
        parent.view.showDialogUpdateSucessfull()
      }

      onException(e: void) {
      }
    })
  }


  getPedidosPorConfirmar() {

    this.pedidoService.getPedidosPorConfirmar().valueChanges().subscribe((data) => {


      if (this.firstTimeEnestablecimiento == false) {

        if (this.numeroRegistrosEnEstablecimiento < data.length) {

          let nuevosregistrosPorCOnfirmar: Array<Pedidon>;
          nuevosregistrosPorCOnfirmar = data;
          let res = nuevosregistrosPorCOnfirmar.filter(item1 =>
            !this.temportalDataEstablecimiento.some(item2 => (item2.id === item1.id)));
          let newPedido: Pedidon = res[0];
          this.view.onNewPedidoPorConfirmar(data, newPedido)

          this.temportalDataEstablecimiento = data;
        }
        else{
          this.view.onNewPedidoPorConfirmar(data)
        }

      }

      if (this.firstTimeEnestablecimiento == true) {
        this.view.onNewPedidoPorConfirmar(data)
        this.temportalDataEstablecimiento = data;
        this.numeroRegistrosEnEstablecimiento = data.length;
        this.firstTimeEnestablecimiento = false;

      }
    });
  }

  setView(view: PedidosPorConfirmarMVP.View) {

    this.view = view
  }


}
