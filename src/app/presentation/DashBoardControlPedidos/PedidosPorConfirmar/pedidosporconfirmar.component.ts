import {Component, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {PedidoDetalle} from 'src/app/domain/entity/pedidodetalle';
import {DashBoardPedidosHelper} from "../../DashBoardAdminEstablecimientos/DashBoardPedidosHelper";

import {MatSort, MatTableDataSource} from "@angular/material";
import {Pedidon} from "../../../domain/entity/Pedidon";
import {TTsServiceService} from "../../../components/services/tts-service.service";
import {PedidosPorConfirmarMVP} from "../../../data/mvpDefinitions/dashboardControlPedidos/PedidosPorConfirmarMVP";

@Component({
  selector: 'app-listapedidosporconfirmar',
  templateUrl: './pedidosporconfirmar.component.html',
  styleUrls: ['./pedidosporconfirmar.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ],
})
export class PedidosporconfirmarComponent implements OnInit, PedidosPorConfirmarMVP.View {

  displayedColumns: string[] = ['nombreEstablecimiento', 'lugarEntrega', 'nombreCliente', 'telefonoCliente','created_at', 'id','buttons'];
  detalles: PedidoDetalle[];

  dataSource: MatTableDataSource<Pedidon>;

  expandedElement: any;
  @ViewChild(MatSort,null) sort: MatSort;
  constructor(
    private presenter: PedidosPorConfirmarMVP.Presenter,
    private ttsService: TTsServiceService,
  ) {
    this.presenter.setView(this)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  setViewStateRetrasado(element: any):Boolean {
    return DashBoardPedidosHelper.setViewStateRetrasado(element)
  }


  loadPedidoDetalle(pedido: any) {
    this.detalles=[]
    this.detalles = pedido.detalles
  }

  actualizarEstadoPedido(idpedido, idestablecimiento, estado) {
    let dataForUpdate = {
      idEst_Estado: idestablecimiento + "_" + estado,
      estado: estado
    }
    this.presenter.updateEstadoDePedido(idpedido, dataForUpdate)
  }

  onDetallesPedidoReady(detalles: PedidoDetalle[]): void {
    this.detalles = detalles
  }

  showDialogUpdateSucessfull(): void {
    console.log('se actualizo correctamente')
  }


  ngOnInit() {

    this.presenter.getPedidosPorConfirmar()
  }
  copyMessageToClipBoard(pedido: Pedidon) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';

    var datatotext = `${pedido.nombreEstablecimiento.toUpperCase()}\n${pedido.nombreCliente} - ${pedido.lugarEntrega} ${pedido.direccionEntrega}\n\n`;
    pedido.detalles.forEach((detalle: PedidoDetalle) => {
      const row = `X${detalle.cantidad} ${detalle.nombreProducto} - S/${detalle.cantidad * detalle.precioVenta}\n`;
      datatotext += row
    })
    datatotext+=`\nSUBTOTAL: S/${pedido.subtotal}`
    selBox.value = datatotext
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
  onNewPedidoPorConfirmar(data: Pedidon[], newPedido?: Pedidon) {

    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    if (newPedido != undefined) {
      this.ttsService
        .speak(`"Se a recibido un nuevo pedido de ${newPedido.nombreCliente}  en el establecimiento ${newPedido.nombreEstablecimiento}`)
    }
  }

}

