import {PedidosService} from "../../../components/services/pedidos.service";
import {Injectable} from "@angular/core";
import {PedidosRechazadosMVP} from "../../../data/mvpDefinitions/dashboardControlPedidos/PedidosRechazadosMVP";

@Injectable()
export class PedidosRechazadosPresenter implements PedidosRechazadosMVP.Presenter {


  private view: PedidosRechazadosMVP.View
  private firstTime: Boolean = true;
  private lastLenght: number;


  constructor(
    private pedidoService: PedidosService,
    private model: PedidosRechazadosMVP.Model
  ) {
  }

  getRechazadosByDate(fecha: any) {

    this.model.getRechazadosByDate(fecha).subscribe(
      pedidosRechazados => {

        const data = pedidosRechazados.filter(pedido => pedido.estado === 'rechazado')
        data.sort((p1, p2) => (p1.created_at < p2.created_at ? -1 : 1))
        //SOLO EMITIRA EL EVENTO EN LA VIEW SI EL NUMERO DE PEDIDOS RECHAZADOS A CRECIDO
        if (this.firstTime) {
          this.view.onPedidosRechazadosChanged(data)
          this.firstTime = false
        } else {
          if (data.length > this.lastLenght) {
            this.view.onPedidosRechazadosChanged(data)
          }
        }
        this.lastLenght = data.length
      })
  }

  setView(view: PedidosRechazadosMVP.View) {
    this.view = view
  }


}
