import {animate, state, style, transition, trigger} from '@angular/animations';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {PedidoDetalle} from 'src/app/domain/entity/pedidodetalle';
import {Pedidon} from "../../../domain/entity/Pedidon";
import {TTsServiceService} from "../../../components/services/tts-service.service";
import {PedidosRechazadosMVP} from "../../../data/mvpDefinitions/dashboardControlPedidos/PedidosRechazadosMVP";
import {Subscription} from "rxjs";
import {DatePipe} from "@angular/common";


@Component({
  selector: 'app-listarpedidosrechazados',
  templateUrl: './pedidos-rechazados.component.html',
  styleUrls: ['./pedidos-rechazados.component.css'],
  providers: [DatePipe],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ],
})
export class PedidosRechazadosComponent implements OnInit, OnDestroy, PedidosRechazadosMVP.View {

  dataSource: MatTableDataSource<Pedidon>;
  displayedColumns: string[] = ['nombreEstablecimiento', 'nombreCliente', 'motivoDeRechazo', 'lugarEntrega', 'telefonoCliente',];
  detalles: PedidoDetalle[];
  expandedElement: any;

  subscription: Subscription

  @ViewChild(MatSort, null) sort: MatSort;
  private firstTime: Boolean = true;
  private lastLenght = 0;

  constructor(
    private presenter: PedidosRechazadosMVP.Presenter,
    private ttsService: TTsServiceService,
    public datepipe: DatePipe
  ) {
    presenter.setView(this)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  onDetallesPedidoReady(result: PedidoDetalle[], newPedido?: Pedidon): void {
    this.detalles = result
  }

  onPedidosRechazadosChanged(data: Pedidon[]): void {
    this.dataSource = new MatTableDataSource<Pedidon>(data)
    this.dataSource.sort = this.sort;

    if (this.firstTime) {
      this.firstTime = false
    } else {
      this.ttsService.speak(`el pedido de ${data[data.length - 1].nombreCliente} en el establecimiento ${data[data.length - 1].nombreEstablecimiento} fué rechazado porque ${data[data.length - 1].motivoDeRechazo}`)
    }


  }

  showInfoDialog(message: string): void {
    console.info(message)
  }

  ngOnInit() {
    const date = this.datepipe.transform(new Date, 'yyyy-MM-dd')
    this.subscription = this.presenter.getRechazadosByDate(date)
  }

  ngOnDestroy(): void {
    if (this.subscription != null)
      this.subscription.unsubscribe()
  }


}
