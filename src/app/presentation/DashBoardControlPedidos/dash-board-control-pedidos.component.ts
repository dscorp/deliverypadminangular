import { Component, OnInit } from '@angular/core';
import {DialogOrdenTransporteApoyoComponent} from "./OrdenesTransporteAtendidas/dialog-orden-transporte-apoyo/dialog-orden-transporte-apoyo.component";
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-dash-board-control-pedidos',
  templateUrl: './dash-board-control-pedidos.component.html',
  styleUrls: ['./dash-board-control-pedidos.component.css']
})
export class DashBoardControlPedidosComponent implements OnInit {

  constructor( public dialog: MatDialog,) { }

  ngOnInit() {
  }

  openDialogOrdTransportApoyo() {
    this.dialog.open(DialogOrdenTransporteApoyoComponent);
  }

}
