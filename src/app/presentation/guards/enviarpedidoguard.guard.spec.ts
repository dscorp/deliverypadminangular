import { TestBed, async, inject } from '@angular/core/testing';

import { EnviarpedidoguardGuard } from './enviarpedidoguard.guard';

describe('EnviarpedidoguardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnviarpedidoguardGuard]
    });
  });

  it('should ...', inject([EnviarpedidoguardGuard], (guard: EnviarpedidoguardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
