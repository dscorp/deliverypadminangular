import {PedidosService} from 'src/app/components/services/pedidos.service';
import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class EnviarpedidoguardGuard implements CanActivate {

  constructor(private pedidoService: PedidosService,
              private router: Router) {

  }

  canActivate(): boolean {


    if (this.pedidoService.pedidosList.length > 0) {
      return true;
    } else {
      this.router.navigateByUrl('/catalogo');
      return false;
    }

  }

}
