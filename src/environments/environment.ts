// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDDBKY6nbP_py1llBqM0rjE32uN5gNPd-U",
    authDomain: "delivery-p.firebaseapp.com",
    databaseURL: "https://delivery-p.firebaseio.com",
    projectId: "delivery-p",
    storageBucket: "delivery-p.appspot.com",
    messagingSenderId: "111749476276",
    appId: "1:111749476276:web:5c973fda98139e14b74ce1",
    measurementId: "G-LSMJ29FJ1B"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
